<!-- static page to be displayed to all the users -->
<html>
    <head>
        <title>About us</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php 
                //session started
                session_start();
                //check if the user is logged in
                if(!empty($_SESSION['id']))
                {
                    // if user is logged in check its user type 
                    if($_SESSION['utype']=="Artist")
                    {
                        //if usertype is artist include header file
                        include 'header.php';
                    }
                    else if($_SESSION['utype']=="Agent"){
                        //if usertype is agent include Agentheader file
                        include 'Agentheader.php';
                    }
                    else {
                        //if usertype is director include Directorheader file
                        include 'Directorheader.php';
                    }
                }
                else {
                    //if not logged in include header file
                    include 'header.php';
                }
            ?>    
            <div class="site-mobile-menu">
              <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                  <span class="icon-close2 js-menu-toggle"></span>
                </div>
              </div>
              <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
            <div class="container">
              <div class="row align-items-center justify-content-center">
                <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                  <h1 class="text-white">About Us</h1>
                </div>
              </div>
            </div>
            </div>

            <div class="site-section">
            <div class="container">
                <div class="row">        
                    <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                        <p>
                          We are the world’s largest creative community of actors, film and TV crew, theatre professionals, child actors, voiceover artists, dancers, singers,
                          musicians, models and extras.<br><br>

                          We empower professionals to find work in TV studios, on movie sets, in training institutions, events, concert halls, theatre companies and art collectives. We
                          help cast commercials, link art directors with cinematographers, and bring lighting designers to dark stages. We support our members as they promote themselves
                          and network across the arts and media industries.<br><br>

                          We champion professionals as they take control of their careers and introduce producers, casting directors and HR managers to
                          the best talent for their projects. We host events and bring together collaborators. We link service providers to clients, artists to agents, aspiring
                          students to colleges... and animal wranglers to dogs.<br><br>

                          Having good contacts is our raison d'être, and we have the history and industry expertise to match. Armed with these qualities, we have established
                          the most dynamic digital career hub for creative talent around the world.<br><br>

                          Audition Magic makes things happen.
                        </p>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <?php 
            //include footer file
            include 'footer.php';
        ?> 
    </body>
</html>
