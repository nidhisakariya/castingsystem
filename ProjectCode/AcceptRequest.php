<?php
session_start();
// check for valid user
if($_SESSION['utype']!="Artist")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
$MERCHANT_KEY = "DiEz4OZQ";
$SALT = "ebGnMmC9Kd";
// Merchant Key and Salt as provided by Payu.

$PAYU_BASE_URL = "https://sandboxsecure.payu.in/_payment";  // For Sandbox Mode
//$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode

$action = '';

$posted = array();
if (!empty($_POST)) {
    //print_r($_POST);
    foreach ($_POST as $key => $value) {
        $posted[$key] = $value;
    }
}

$formError = 0;

if (empty($posted['txnid'])) {
    // Generate random transaction id
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
    $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if (empty($posted['hash']) && sizeof($posted) > 0) {
    if (
            empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['productinfo']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])
    ) {
        $formError = 1;
    } else {
        //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';
        foreach ($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string .= $SALT;


        $hash = strtolower(hash('sha512', $hash_string));
        $action = $PAYU_BASE_URL . '/_payment';
    }
} elseif (!empty($posted['hash'])) {
    $hash = $posted['hash'];
    $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
    <head>
        <title>Accept Request</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body onload="submitPayuForm()">
        <div class="site-wrap">
        <?php include 'header.php'; ?>    
        <div class="site-mobile-menu">
          <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
              <span class="icon-close2 js-menu-toggle"></span>
            </div>
          </div>
          <div class="site-mobile-menu-body"></div>
        </div> <!-- .site-mobile-menu -->

        <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white">Terms & Conditions Agreement</h1>
            </div>
          </div>
        </div>
        </div>

        <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                    <p>
                        I hereby agree that the agent is not responsible with complete guarantee of getting a job by hiring him.

                        I agree to pay the one time fee to the agent for hiring him as a representative of my work and skills.

                        I give him the rights to share my profile with the registered casting calls as and when needed.

                        I am liable to pay him a commission after selection as agreed by both the parties.

                        I agree to share my information with the agent.
                    </p>
                    <input type="checkbox" id="agree"><strong>I agree with the above terms & conditions</strong>
                    <br>
                    <br>
                    <form action="<?php echo $action; ?>" method="post" name="payuForm">
                        <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                        <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                        <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                        <input type="hidden" name="amount" value=<?php echo $_SESSION['Amount'];?> />
                        <input type="hidden" name="firstname" id="firstname" value="Nidhi" />
                        <textarea name="productinfo" style="display:none;">payment</textarea>
                        <input type="hidden" name="email" id="email" value="nidhisakariya98@gmail.com" /></td>
                        <input type="hidden" name="phone" value="7069826216" /></td>
                        <input type="hidden" name="surl" value="http://localhost/CastingProject/success.php"/></td>
                        <input type="hidden" name="furl" value="http://localhost/CastingProject/failure.php" />
                        <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                        <?php if (!$hash) { ?>
                          <input type="submit"  value="Accept&Proceed"  id="save" class="btn btn-primary py-3 px-4"/>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
        </div>
        <?php
            // include footer file
            include 'footer.php';
        ?> 
        <script>
            var hash = '<?php echo $hash ?>';
            function submitPayuForm() {
                if (hash == '') {
                    return;
                }
                var payuForm = document.forms.payuForm;
                payuForm.submit();
            }
        </script>
        <!-- script to display proceed button only when terms and conitions checkbox is checked-->
        <script type="text/javascript"> 
            $(document).ready(function() {
                $('#save').hide();
            });

            $(function() {
                $('#agree').click(function() {
                    $('#save').toggle();
                });
            });
        </script>
        </div>
    </body>
</html>
