<?php
//This page is to send notification about acceptance for audition by director to agent and artist
session_start();
//checkig for logged in status
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 </head>
    <body>
          <div class="modal fade" id="successModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                    <a href="index.php"><button type="button" class="close">&times;</button></a>
                  <h4 class="modal-title" style="color:green">Success</h4>
                </div>
                <div class="modal-body">
                  <p>Audition call request sent!</p>
                </div>
                <div class="modal-footer">
                    <a href="index.php"><button type="button" class="btn btn-success">Ok</button></a>
                </div>
              </div>
            </div>
          </div>
        
         <div class="modal fade" id="failModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="color:red">Failed</h4>
                </div>
                <div class="modal-body">
                  <p>Error in fetching data!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><a href="index.php">Close</a></button>
                </div>
              </div>
            </div>
          </div>
        
        <div class="modal fade" id="failChangeModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="color:red">Failed</h4>
                </div>
                <div class="modal-body">
                  <p>Audition call sending failed! Try again later!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><a href="index.php">Close</a></button>
                </div>
              </div>
            </div>
          </div>

        <!--<script src="js/blurt.js"></script>-->
        <script>
            $('#successModal').modal({backdrop: 'static', keyboard: false}) 
        </script>
    </body>
</html>
<?php
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else {
    // include database connection file
    include 'connection.php';
    //query to select agents name
    $selectagent="select AgentName,AgentId from tbl_agents_master where UserId='".$_GET['agtid']."'";
    $name= mysqli_query($con, $selectagent);
    $agtname= mysqli_fetch_array($name,MYSQLI_ASSOC);
    if(!$name)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    //query to select artists name
    $selectartist="select Name,ArtistId from tbl_artist_master where UserId='".$_GET['artid']."'";
    $aname= mysqli_query($con, $selectartist);
    $artname= mysqli_fetch_array($aname,MYSQLI_ASSOC);
    if(!$aname)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    //message to be sent which will be displayed in notification bar
    $msgbodyforartist = "Congratulations! You have been select for the auditions for the " . $_SESSION['body'] ." send by ".$agtname['AgentName'];
    $msgbodyforagent= "Congratulations! Your client ".$artname['Name']." is selected for the auditions of ".$_SESSION['body'];
    //calling notification stored procedure        
    $callNotify = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
    mysqli_stmt_bind_param($callNotify, 'iiss', $_SESSION['id'], $_GET['artid'], $msgbodyforartist, date("Y-m-d H:i:s"));//date() to fetch current date
    mysqli_stmt_execute($callNotify);
    //calling notification stored procedure
    $callNotify2 = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
    mysqli_stmt_bind_param($callNotify2, 'iiss', $_SESSION['id'], $_GET['agtid'], $msgbodyforagent, date("Y-m-d H:i:s"));
    mysqli_stmt_execute($callNotify2);
    
    $selectId="select JobId from tbl_postjob where RoleDescription='".$_SESSION['body']."'";
    $res= mysqli_query($con, $selectId);
    $job= mysqli_fetch_array($res,MYSQLI_ASSOC);
    
    $update="update tbl_applyjob set SelectStatus=1 where AgentId='".$agtname['AgentId']."' and ArtistId='".$artname['ArtistId']."' and JobId='".$job['JobId']."'";
    $status= mysqli_query($con, $update);
    if(!$status)
    {
        echo "<script>$('#failChangeModal').modal('show')</script>";
    }
    else {
          echo "<script>$('#successModal').modal('show')</script>";
    }
}
?>