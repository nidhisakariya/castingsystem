<?php
//To add the video of artists skill showcase
session_start();
//checking for loggin status
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
//checkin for user type to allow page access
if ($_SESSION['utype'] != "Artist") {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
} 
$Ftype = "";
if (isset($_POST['btn_register'])) {
    $state = 1;

    //fetching video extension
    $extensionVideo = pathinfo($_FILES["video"]["name"], PATHINFO_EXTENSION);
    //for changing name of video file so that no overwriting takes place
    $nameVideo = "vid" . $_SESSION['id'] . $_FILES["video"]["name"];
    $targetDirVideo = "Videos/";
    $fileNameVideo = "$nameVideo";
    $targetFilePathVideo = $targetDirVideo . $fileNameVideo;
    $fileTypeVideo = pathinfo($targetFilePathVideo, PATHINFO_EXTENSION);
    $allowTypesVideo = array('mp4', 'mpg', 'mp2');
    //check file existence
    if (!file_exists($_FILES["video"]["tmp_name"])) {
        $Ftype = "File does not exist!";
    }    // Validate file input to check if is with valid extension
    else if (!in_array($extensionVideo, $allowTypesVideo)) {
        $Ftype = "You can add only mp4,mpg or mp2 files!";
    } else {
        include 'connection.php';
        // call stored procedure to add video
        $callCreate = mysqli_prepare($con, 'CALL AddVideo(?, ?, ?, ?)');
        mysqli_stmt_bind_param($callCreate, 'issi', $_SESSION['id'], $fileNameVideo, $_POST['description'], $state);
        mysqli_stmt_execute($callCreate);
        //move video file to folder
        move_uploaded_file($_FILES["video"]["tmp_name"], $targetFilePathVideo);

        echo "<script>location.href='ArtistProfile.php';</script>";
    }
}
?>
<html>
    <head>
        <title>Add Video</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Add Video</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" class="contact-form" enctype="multipart/form-data"> 
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="vdo" style="color: #e3c4a8;">Video:</label><br>
                                    <input type="file" name="video"  required="required"><br>
                                    <label style="color: red"><?php echo $Ftype; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="desc" style="color: #e3c4a8;">Video Description:</label>
                                    <textarea name="description" class="form-control" style="color: black;width: 400px;" maxlength="500" placeholder="Enter video description" pattern="^[a-zA-Z,.!? ]*$" title="Only alphabets , . ? ! are allowed!"></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Save" name="btn_register" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>
                    </center>
                </div>
            </div>
            <?php include 'footer.php'; ?> 
    </body>
</html>
