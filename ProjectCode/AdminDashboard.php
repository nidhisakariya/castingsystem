<?php
//Dasboard of admin displays some numeric reports
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="login.php"</script>';
}
else if($_SESSION['utype']!="Admin")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //including admin's header file
        include 'AdminHeader.php';
        //for the purpose of validation
        $artists="";
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>
                </div>
                    <?php 
                       //database connectivity
                       include 'connection.php';
                       // counting total number of artist's registered 
                       $q="SELECT COUNT(*) FROM tbl_users WHERE UserType='Artist'";
                       $result= mysqli_query($con, $q);
                       $row = mysqli_fetch_assoc($result);
                       $size = $row['COUNT(*)'];
                    ?>
                <!-- display no. of artists -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex p-10 no-block">
                                    <div class="align-slef-center">
                                        <h2 class="m-b-0"><?php echo "$size" ?></h2>
                                        <h6 class="text-muted m-b-0">Number Of Artists</h6>
                                    </div>
                                    <div class="align-self-center display-6 ml-auto"><i class="text-success icon-Target-Market"></i></div>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:3px;"> <span class="sr-only">10% Complete</span></div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        // counting total number of casting directors registered 
                        $q1="SELECT COUNT(*) FROM tbl_users WHERE UserType='Director'";
                        $result1= mysqli_query($con, $q1);
                        $row1 = mysqli_fetch_assoc($result1);
                        $size1 = $row1['COUNT(*)'];
                    ?>
                    <!-- displaying total no. of directors -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex p-10 no-block">
                                    <div class="align-slef-center">
                                        <h2 class="m-b-0"><?php echo "$size1" ?></h2>
                                        <h6 class="text-muted m-b-0">Number Of Directors</h6>
                                    </div>
                                    <div class="align-self-center display-6 ml-auto"><i class="text-success icon-Target-Market"></i></div>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:3px;"> <span class="sr-only">50% Complete</span></div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        // select total no of agents registered
                        $q2="SELECT COUNT(*) FROM tbl_users WHERE UserType='Agent'";
                        $result2= mysqli_query($con, $q2);
                        $row2 = mysqli_fetch_assoc($result2);
                        $size2= $row2['COUNT(*)'];
                    ?>
                    <!-- displayno. of agents -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex p-10 no-block">
                                    <div class="align-slef-center">
                                        <h2 class="m-b-0"><?php echo"$size2" ?></h2>
                                        <h6 class="text-muted m-b-0">Number Of Agents</h6>
                                    </div>
                                    <div class="align-self-center display-6 ml-auto"><i class="text-success icon-Target-Market"></i></div>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:3px;"> <span class="sr-only">50% Complete</span></div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        // total payment collected from artists registration till date
                        $q3="SELECT SUM(Amount) as totalAmount FROM tbl_payment";
                        $result3= mysqli_query($con, $q3);
                        $row3 = mysqli_fetch_assoc($result3);
                        $size3 = $row3['totalAmount'];
                    ?>
                    <!-- displaying total payment collected -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex p-10 no-block">
                                    <div class="align-slef-center">
                                        <h2 class="m-b-0"><?php echo "$size3"; ?></h2>
                                        <h6 class="text-muted m-b-0">Total Registration Fee</h6>
                                    </div>
                                    <div class="align-self-center display-6 ml-auto"><i class="text-info icon-Dollar-Sign"></i></div>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:3px;"> <span class="sr-only">50% Complete</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>