<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>AdminWrap - Easy to Customize Bootstrap 4 Admin Template</title>
        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/" />
        <!-- Bootstrap Core CSS -->
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" rel="stylesheet">
        <!-- This page CSS -->
        <!-- chartist CSS -->
        <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">
        <!-- Vector CSS -->
        <link href="assets/node_modules/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
        <!--c3 CSS -->
        <link href="assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style_1.css" rel="stylesheet">
        <!-- Dashboard 1 Page CSS -->
        <link href="css/pages/dashboard2.css" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <div id="main-wrapper"></div>
            <header class="topbar">
                <nav class="navbar top-navbar navbar-expand-md navbar-light">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="AdminDashboard.php">
                            <b>
                                <img src="assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            </b>
                            <span>
                                <img src="assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                            </span>
                        </a>
                    </div>
                    
                    <div class="navbar-collapse">                       
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>
                            <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="sl-icon-menu"></i></a> </li>                            
                        </ul>
                        
                        <ul class="navbar-nav my-lg-0">                            
                            <li class="nav-item dropdown u-pro">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="hidden-md-down">Admin&nbsp;<i class="fa fa-angle-down"></i></span> </a>
                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                    <ul class="dropdown-user">
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                        <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="Logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            
            <aside class="left-sidebar">
                <div class="scroll-sidebar">
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                            <li class="nav-small-cap"></li>
                            <li> <a href="AdminDashboard.php" aria-expanded="false"><i class="icon-Car-Wheel"></i>Dashboard</a>
                                
                            </li>
                            <li> <a  href="RegisterAgent.php" aria-expanded="false"><i class="icon-Double-Circle"></i>Register Agent</a>
                                
                            </li>
                            <li> <a href="Monologues.php" aria-expanded="false"><i class="icon-Box-Full"></i>Monologues</a>
                                
                            </li>
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-On-Off-3"></i><span class="hide-menu">Users <span class="label label-rounded label-success">3</span></span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="ArtistInfoAdmin.php">Artists</a></li>
                                    <li><a href="DirectorInfoAdmin.php">Directors</a></li>
                                    <li><a href="AgentInfoAdmin.php">Agents</a></li>
                                </ul>
                            </li>
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-Receipt-4"></i><span class="hide-menu">Reports<span class="label label-rounded label-success">7</span></span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="ArtistInvitedReport.php">Artists Invited</a></li>
                                    <li><a href="DirectorRegisteredReport.php">Registered Casting Directors</a></li>
                                    <li><a href="AgentsRegisteredReport.php">Registered Agents</a></li>
                                    <li><a href="ArtistRegisteredReport.php">Registered Artists</a></li>
                                    <li><a href="ClientsMadeReport.php">Clients Made By Agents</a></li>
                                    <li><a href="AgentFeeReport.php">Agent Subscription Fee</a></li>
                                    <li><a href="RegistrationFee.php">Registration Fee</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>                   
                </div>    
            </aside>
        
            <script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <script src="js/custom.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--morris JavaScript -->
            <script src="assets/node_modules/raphael/raphael.min.js"></script>
            <script src="assets/node_modules/morrisjs/morris.min.js"></script>
            <!--c3 JavaScript -->
            <script src="assets/node_modules/d3/d3.min.js"></script>
            <script src="assets/node_modules/c3-master/c3.min.js"></script>
            <!-- Vector map JavaScript -->
            <script src="assets/node_modules/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
            <script src="ssets/node_modules/vectormap/jquery-jvectormap-world-mill-en.js"></script>
            <!-- Chart JS -->
            <script src="js/dashboard2.js"></script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>