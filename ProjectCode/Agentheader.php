<!-- Agent's header -->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Oswald:400,700"> 
        <link rel="stylesheet" href="fonts/icomoon/style.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="css/mediaelementplayer.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
        <link rel="stylesheet" href="css/fl-bigmug-line.css">
        <link rel="stylesheet" href="css/aos.css">
        <link rel="stylesheet" href="css/style.css">
        <style>
            #count{
                border-radius: 50%;
                position: relative;
                top: -10px;
                left: -10px;
            }
        </style>
    </head>
    <body>
        <div class="site-navbar mt-4">
        <div class="container py-1">
            <div class="row align-items-center">
                <div class="col-8 col-md-8 col-lg-4">
                    <h1 class="mb-0"><a href="index.php" class="text-white h2 mb-0"><strong>Casting Magic<span class="text-primary">.</span></strong></a></h1>
                </div>
                <div class="col-4 col-md-4 col-lg-8">
                    <nav class="site-navigation text-right text-md-right" role="navigation">
                    <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
                    <ul class="site-menu js-clone-nav d-none d-lg-block">
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="has-children">
                            <a href="#">My Account</a>
                            <ul class="dropdown arrow-top">
                                <li><a href="CreateAgentProfile.php">My profile</a></li>
                                <li><a href="AgentRoster.php">My Roaster</a></li>
                            <?php
                            if(!empty($_SESSION['id']))
                            {
                            ?>
                                <li><a href="logout.php"><input type="button" value="Logout" class="btn btn-primary px-4"></a></li>
                            <?php   
                            }                  
                            ?>
                            </ul>
                        </li>
                        <li class="has-children">
                            <a href="#">Clients</a>
                            <ul class="dropdown arrow-top">
                              <li><a href="FindClients.php">Reach Talent</a></li>
                              <li><a href="FindJobsAgent.php">Find job</a></li>
                              <li><a href="HowAgent.php">How it works</a></li>
                              <?php
                            if(empty($_SESSION['id']))
                            {
                            ?>
                                <li><a href="registration.php"><input type="button" value="Join" class="btn btn-primary px-4"></a></li>
                            <?php 
                            }                  
                            ?>
                            </ul>
                        </li>
                        <li><a href="Aboutus.php">About</a></li>
                        <li><a href="ContactUs.php">Contact</a></li>     
                            <?php
                                $count = ""; 
                                //checking if session is empty or not 
                                if (!empty($_SESSION['id'])) {
                                    include 'connection.php';
                                    $q = "select * from tbl_notification where RecieverId='" . $_SESSION['id'] . "' and Status=0";
                                    $result = mysqli_query($con, $q);
                                    $count = mysqli_num_rows($result);

                                    if ($count == 0) {
                                        $count = "";
                                    }
                                }
                                ?>
                                <li class="has-children">
                                    <a href="#">
                                        <i class="fl-bigmug-line-notification4"></i>
                                        <span class="badge badge-light" id="count">
                                            <?php 
                                                //displayig total notifications of the users in badge
                                                echo $count;
                                            ?>
                                        </span>
                                        <ul class="dropdown arrow-top">
                                            <?php
                                            if ($count != 0) {
                                                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                                                    //fetching senders user type
                                                    $selectType = "select UserType from tbl_users where UserId='" . $row['SenderId'] . "'";
                                                    $result = mysqli_query($con, $selectType);
                                                    $rowtype = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                                    //checking if its agent
                                                    if ($rowtype['UserType'] == "Artist") {
                                                        //fetch agents name
                                                        $selectName = "select Name from tbl_artist_master where UserId='" . $row['SenderId'] . "'";
                                                        $resultName = mysqli_query($con, $selectName);
                                                        $rowName = mysqli_fetch_array($resultName, MYSQLI_ASSOC);
                                                        $_SESSION['call']=$rowName['Name']." has ".$row['NotificationBody'];
                                                        ?> <li><a href="ClientPage.php?uid=<?php echo $row['SenderId'];?>"><strong>ARTIST<br><?php echo $rowName['Name']; ?></strong><br><?php echo $row['NotificationBody']; ?></a></li>
                                                        <?php
                                                    } else {
                                                        //if user is director fetch his company name
                                                        $selectName = "select CompanyName from tbl_castingdirector_master where UserId='" . $row['SenderId'] . "'";
                                                        $resultName = mysqli_query($con, $selectName);
                                                        $rowName = mysqli_fetch_array($resultName, MYSQLI_ASSOC);
                                                        $_SESSION['call']=$row['NotificationBody'];
                                                        ?> <li><a href="AuditionCallAgent.php?uid=<?php echo $row['SenderId'];?>"><strong>COMPANY<?php echo $rowName['CompanyName']; ?></strong><br><?php echo $row['NotificationBody']; ?></a></li>
                                                        <?php
                                                    }
                                                    echo '<hr>';
                                                    mysqli_close($con);
                                                }
                                            }
                                                if(!empty($_SESSION['id']))
                                                {
                                                    //if user is logged in display all notifications tab and fetch all the notification till date
                                                    include 'connection.php';
                                                    $qu1 = "select * from tbl_notification where RecieverId='" . $_SESSION['id'] . "'";
                                                    $result1 = mysqli_query($con, $qu1);
                                                    $cnt= mysqli_num_rows($result1);                                                   
                                                    if($cnt!=0)
                                                    {
                                                        $rows = mysqli_fetch_array($result1, MYSQLI_ASSOC);                                                    
                                                ?>                                                       
                                                <li><a href="AllNotificationsAgent.php">All notifications</a></li>
                                                <?php
                                                    }
                                                    mysqli_close($con);
                                                }
                                                ?>                                           
                                        </ul>
                                    </a>
                                </li>
                            <?php
                                if(!empty($_SESSION['id']))
                                {
                                        include 'connection.php';
                                        $query="select * from chat_message where to_user_id='".$_SESSION['id']."' and status=1";
                                        $fetch= mysqli_query($con, $query);
                                        $countmessage= mysqli_num_rows($fetch);
                                        if($countmessage==0)
                                        {
                                            $countmessage="";
                                        }
                                ?>
                                <li><a href="chat.php"><i class="fl-bigmug-line-chat55"></i><span class="badge badge-light" id="count">
                                            <?php echo $countmessage; ?></span></a></li>
                                <?php
                                }
                                else {
                                ?>
                                <li><a href="#"><i class="fl-bigmug-line-chat55"></i></a></li>
                                <?php
                                }
                                ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
