<?php
//display all the Agents info to admin 
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="login.php"</script>';
}
else if($_SESSION['utype']!="Admin")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
        <?php
        // include header
        include 'AdminHeader.php';
        include 'database_connection.php';
        include 'connection.php';
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Registered Agents Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Reports</a></li><li class="breadcrumb-item active">Registered Agent</li>
                        </ol>
                    </div>
                    <?php 
                        // select total no of agents registered
                        $q2="SELECT COUNT(*) FROM tbl_users WHERE UserType='Agent'";
                        $result2= mysqli_query($con, $q2);
                        $row2 = mysqli_fetch_assoc($result2);
                        $size2= $row2['COUNT(*)'];
                    ?>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>
                </div>
                <div class="row" id="foot">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3> Total: <?php echo $size2 ?> </h3>
                                    <table class="table table-bordered m-t-30 table-hover contact-list footable footable-1 footable-paging footable-paging-center breakpoint-sm">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Contact Number</th>
                                                <th>Email Id</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                               
                                                $con = mysql_connect('localhost', 'root', '');
                                                mysql_select_db('casting_system');
                                                //get the status of agent
                                                $query = "SELECT UserId,AdminStatus FROM tbl_users WHERE Status=1 and UserType='Agent'";
                                                $execute = mysql_query($query);
                                                //fetch one by one
                                                while ($rowinfo = mysql_fetch_assoc($execute)) {
                                                    //get agents information
                                                    $selectSQL = "SELECT AgentName,ContactNo,EmailId,NoOfClients,NoOfClientsCasted,Experience,Fees FROM tbl_agents_master WHERE UserId='" . $rowinfo['UserId'] . "'";
                                                    if (!( $selectRes = mysql_query($selectSQL) )) {
                                                        echo 'Retrieval of data from Database Failed - #' . mysql_errno() . ': ' . mysql_error();
                                                    } else {
                                                        if (mysql_num_rows($selectRes) == 0) {
                                                               //empty if no agents registered 
                                                        } else {
                                                            //fetch each agent one by one
                                                            while ($row = mysql_fetch_assoc($selectRes)) {
                                                                echo "<tr><td>{$row['AgentName']}</td>"
                                                                . "<td>{$row['ContactNo']}</td>"
                                                                . "<td>{$row['EmailId']}</td>"
                                                                
                                                                ?>
                                                                
                                                                <?php
                                                                      }
                                                                "</td></tr>\n";
                                                            }
                                                        }
                                                    }
                                                
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>