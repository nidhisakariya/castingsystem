<?php
// to display all notifications of artist
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <title>All Notifications Artist</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Notifications</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <div class="row">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sender</th>
                                    <th>Notification</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_notification WHERE RecieverId='".$_GET['uid']."' order by CreateDate desc";
                                $execute = mysql_query($query);
                                //fetch data one by one
                                while ($row = mysql_fetch_assoc($execute)) {
                                    //fetch the senders user type
                                    $selectSQL = "SELECT UserType from tbl_users where UserId='".$row['SenderId']."'";
                                    $selectRes=mysql_query($selectSQL);
                                    $rowtype = mysql_fetch_assoc($selectRes);
                                    if ($rowtype['UserType'] == "Agent") {
                                        //if sender is agent fetch his name
                                        $selectName="select AgentName from tbl_agents_master where UserId='".$row['SenderId']."'";
                                        $resultName=mysql_query($selectName);
                                        $rowName=mysql_fetch_assoc($resultName);
                                        //display all notifications in tabular format
                                        echo "<tr><td>{$rowName['AgentName']}</td>"
                                        . "<td>{$row['NotificationBody']}</td>"
                                        . "<td>{$row['CreateDate']}</td>"
                                        . "<td><a href=ViewAgentProfile.php?uid={$row['SenderId']}>View Profile</a></td>"
                                        . "<tr>\n";
                                    }
                                    else {
                                        //if user type of sender is director fetch his company name
                                        $selectName="select CompanyName from tbl_castingdirector_master where UserId='".$row['SenderId']."'";
                                        $resultName=mysql_query($selectName);
                                        $rowName=mysql_fetch_assoc($resultName);
                                        //display all notifications sent by this casting director
                                        echo "<tr><td>{$rowName['CompanyName']}</td>"
                                        . "<td>{$row['NotificationBody']}</td>"
                                        . "<td>{$row['CreateDate']}</td>"
                                        . "<td></td><tr>\n";
                                    }                                   
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php
                //pagination concept
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 10;
                $offset = ($pageno - 1) * $no_of_records_per_page;

                include 'connection.php';
                $total_pages_sql = "SELECT COUNT(*) FROM tbl_notification where RecieverId ='".$_SESSION['id']."'";
                $result = mysqli_query($con, $total_pages_sql);
                $total_rows = mysqli_fetch_array($result)[0];
                $total_pages = ceil($total_rows / $no_of_records_per_page);

                $sql = "SELECT * FROM tbl_notification LIMIT $offset, $no_of_records_per_page";
                $res_data = mysqli_query($con, $sql);
                while ($row = mysqli_fetch_array($res_data)) {
                    //here goes the data
                }
                ?>
                <div class="container mt-5 aos-init aos-animate" data-aos="fade-up">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="site-block-27">
                                <ul>
                                    <li><a href="?pageno=1">1</a></li>
                                    <li class="<?php if ($pageno <= 1) {
                                            echo 'disabled';
                                        } ?>">
                                        <a href="<?php if ($pageno <= 1) {
                                            echo '#';
                                        } else {
                                            echo "?pageno=" . ($pageno - 1);
                                        } ?>"><</a>
                                    </li>
                                    <li class="<?php if ($pageno >= $total_pages) {
                                            echo 'disabled';
                                        } ?>">
                                        <a href="<?php if ($pageno >= $total_pages) {
                                        echo '#';
                                    } else {
                                        echo "?pageno=" . ($pageno + 1);
                                    } ?>">></a>
                                    </li>
                                    <li><a href="?pageno=<?php echo $total_pages; ?>">10</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?> 
    </body>
</html>
