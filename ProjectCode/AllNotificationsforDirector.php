<?php
//This page is created to display all the notification of diirectors that have been recieved uptil date, which are read or unread.
session_start();
// Checking for the user type to access the page
if($_SESSION['utype']!="Director")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <title>Notification Director</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php 
                //including the header file while contains header according to the user type
                include 'Directorheader.php';
            ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Notifications</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <div class="row">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sender</th>
                                    <th>Notification</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                //database connectivity using mysql
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                
                                //query to select notification information of whole table
                                $NotiInfo = "SELECT  * FROM tbl_notification WHERE RecieverId='".$_SESSION['id']."' order by CreateDate desc";
                                $getinfo = mysql_query($NotiInfo);
                                
                                //fetch each row one by one
                                while ($row = mysql_fetch_assoc($getinfo)) {
                                    //select usertype of sender's field in tbl_notification
                                    $UserType = "SELECT UserType from tbl_users where UserId='".$row['SenderId']."'";
                                    $gettype=mysql_query($UserType);
                                    $rowforType = mysql_fetch_assoc($gettype);
                                    
                                    //checking if the usertype is artist
                                    if ($rowforType['UserType'] == "Artist") {
                                        //select name of the artist to display in the table column
                                        $selectName="select Name from tbl_artist_master where UserId='".$row['SenderId']."'";
                                        $getName=mysql_query($selectName);
                                        $rowforName=mysql_fetch_assoc($getName);
                                        
                                        //$_SESSION['body']=$row['NotificationBody'];
                                        //display the necessary info in a tabular format
                                        echo "<tr><td>{$rowforName['Name']}</td>"
                                        . "<td>{$row['NotificationBody']}</td>"
                                        . "<td>{$row['CreateDate']}</td>" 
                                        //a link which redirects to display artist's information with artists user id
                                        . "<td><a href=ViewArtistProfileForDirector.php?uid={$row['SenderId']}>View Profile</a></td>"
                                        . "<tr>\n";
                                    } else {
                                        //if user type is agent
                                        //select the agent's name
                                        $selectName="select AgentName from tbl_agents_master where UserId='".$row['SenderId']."'";
                                        $getName=mysql_query($selectName);
                                        $rowforName=mysql_fetch_assoc($getName);
                                        
                                        //display the necessary info in a tabular format
                                        echo "<tr><td>{$rowforName['AgentName']}</td>"
                                        . "<td>{$row['NotificationBody']}</td>"
                                        . "<td>{$row['CreateDate']}</td>"
                                        //a link that redirector to view all the profiles sent by agent with agents user id
                                        . "<td><a href=JobApplicationsbyAgent.php?uid={$row['SenderId']}>View </td>"
                                        . "<tr>\n";
                                    }                                   
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php
                //pagination concept
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 10;
                $offset = ($pageno - 1) * $no_of_records_per_page;

                include 'connection.php';
                $total_pages_sql = "SELECT COUNT(*) FROM tbl_notification where RecieverId='".$_SESSION['id']."'";
                $result = mysqli_query($con, $total_pages_sql);
                $total_rows = mysqli_fetch_array($result)[0];
                $total_pages = ceil($total_rows / $no_of_records_per_page);

                $sql = "SELECT * FROM tbl_notification LIMIT $offset, $no_of_records_per_page";
                $res_data = mysqli_query($con, $sql);
                while ($row = mysqli_fetch_array($res_data)) {
                    //here goes the data
                }
            ?>
            <div class="container mt-5 aos-init aos-animate" data-aos="fade-up">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="site-block-27">
                            <ul>
                                <li><a href="?pageno=1">1</a></li>
                                <li class="<?php 
                                    if ($pageno <= 1) {
                                        echo 'disabled';
                                    } ?>">
                                        <a href="<?php if ($pageno <= 1) {
                                        echo '#';
                                    } else {
                                        echo "?pageno=" . ($pageno - 1);
                                    } ?>"><</a>
                                </li>
                                <li class="<?php if ($pageno >= $total_pages) {
                                        echo 'disabled';
                                    } ?>">
                                        <a href="<?php if ($pageno >= $total_pages) {
                                        echo '#';
                                    } else {
                                        echo "?pageno=" . ($pageno + 1);
                                    } ?>">></a>
                                </li>
                                <li><a href="?pageno=<?php echo $total_pages; ?>">10</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <?php
            //including footer file
            include 'footer.php'; 
        ?> 
    </body>
</html>
