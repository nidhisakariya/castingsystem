<?php
session_start();
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 </head>
    <body>
<div class="modal fade" id="successModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                    <a href="OpenJobsDisplay.php"><button type="button" class="close">&times;</button></a>
                  <h4 class="modal-title" style="color:green">Success</h4>
                </div>
                <div class="modal-body">
                  <p>Job application sent!</p>
                </div>
                <div class="modal-footer">
                    <a href="OpenJobsDisplay.php"><button type="button" class="btn btn-success">Ok</button></a>
                </div>
              </div>
            </div>
          </div>
        
         <div class="modal fade" id="failModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="color:red">Failed</h4>
                </div>
                <div class="modal-body">
                  <p>Job application failed to sent! Try again later!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><a href="index.php">Close</a></button>
                </div>
              </div>
            </div>
          </div>
        <!--<script src="js/blurt.js"></script>-->
        <script>
            $('#successModal').modal({backdrop: 'static', keyboard: false}) 
        </script>
    </body>
</html>
<?php
if(empty($_SESSION['id']))
{
    echo '<script>location.href="Login.php";</script>';
}
else
{
    include 'connection.php';
    $query="select RoleDescription from tbl_postjob where JobId='". $_GET['job']."'";
    $result= mysqli_query($con, $query);
    $row= mysqli_fetch_array($result,MYSQLI_ASSOC);
    if(!$result)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
   
    $artistid="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
    $re2= mysqli_query($con, $artistid);
    $r2= mysqli_fetch_array($re2,MYSQLI_ASSOC);
    if(!$re2)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    
    $msgbody="Job Application for, ".$row['RoleDescription'];   
    $callNotify = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
    mysqli_stmt_bind_param($callNotify, 'iiss', $_SESSION['id'], $_GET['uid'], $msgbody,date("Y-m-d H:i:s"));
    mysqli_stmt_execute($callNotify);
    if(!$callNotify)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    mysqli_close($con);
    include 'connection.php';
    $callApply= mysqli_prepare($con, 'CALL applybyartist(?, ?, ?)');
    mysqli_stmt_bind_param($callApply, 'iii',$r2['ArtistId'],$_GET['did'],$_GET['job']);
    mysqli_stmt_execute($callApply);
    if(!$callApply)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    else {
        echo "<script>$('#successModal').modal('show')</script>";
    }
    mysqli_close($con);
}
?>