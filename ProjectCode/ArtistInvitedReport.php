<?php
//display all the Agents info to admin
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="login.php"</script>';
} else if ($_SESSION['utype'] != "Admin") {
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
<?php
// include header
include 'AdminHeader.php';
//include 'database_connection.php';
//include 'connection.php';
?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Artists Invited Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a
                                    href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a
                                    href="javascript:void(0)">Reports</a></li><li class="breadcrumb-item
                                                                          active">Artists Invited</li>
                        </ol>
                    </div>


                    <div class="">
                        <button class="right-side-toggle waves-effect
                                waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
                <div class="row" id="foot">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table class="table table-bordered
                                           m-t-30 table-hover contact-list footable footable-1 footable-paging
                                           footable-paging-center breakpoint-sm">
                                        <thead>
                                            <tr>
                                                <th>Artist Name</th>
                                                <th>Company Name</th>
                                                <th>Role Type</th>
                                               

                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$con = mysql_connect('localhost', 'root', '');

mysql_select_db('casting_system');
//get the status of agent

$query = "select * from
tbl_applyjob where SelectStatus = 1";
$result = mysql_query($query);
//fetch one by one
while ($row = mysql_fetch_array($result)) {
    //get agents information
    $artist = "select * from tbl_artist_master where ArtistId='" . $row['ArtistId'] . "'";
    $artistid = mysql_query($artist);
    $id = mysql_fetch_array($artistid, MYSQLI_ASSOC);
   
    $company="select CompanyName from tbl_castingdirector_master where DirectorId='".$row['DirectorId']."'";
    $comp=mysql_query($company);
    $com = mysql_fetch_array($comp, MYSQLI_ASSOC);
   
    $role="Select RoleType from tbl_postjob where JobId='".$row['JobId']."' ";
    $roles=mysql_query($role);
    $rol = mysql_fetch_array($roles, MYSQLI_ASSOC);

    echo
    "<tr><td>{$id['Name']}</td>"
    .
    "<td>{$com['CompanyName']}</td>"
    .
    "<td>{$rol['RoleType']}</td>"
   
    .
    "</td></tr>\n";
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

