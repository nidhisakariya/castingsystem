<?php
session_start();
if (empty($_SESSION['id'])) {
        echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
?>
<html>
    <head>
        <title>Artist Profile</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <!-- Column -->
                <div style="padding-left:200px;" class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30">
                                <?php
                                    include 'connection.php';
                                    $q = "select * from tbl_artist_master where UserId='".$_SESSION['id']."'";
                                    $result = mysqli_query($con, $q);
                                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                    if(!$result)
                                    {
                                        echo mysqli_error($con);
                                    }    
                                    mysqli_close($con);
                                ?>
                                <img src="ProfilePhotos/<?php echo $row['ProfilePhoto']; ?>" class="img-circle" alt="Image" width="150" />
                                <br><h4 class="card-title m-t-10"><?php echo $row['Name']; ?></h4>
                            </center>
                        </div>
                        <div>
                            <hr> </div>
                        <div class="card-body"> <small class="text-muted">Email address </small>
                            <h6><?php echo $row['EmailId']; ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                            <h6><?php echo $row['ContactNo']; ?></h6> <small class="text-muted p-t-30 db">Address</small>
                            <h6><?php echo $row['Address']; ?></h6>
                            <small class="text-muted p-t-30 db">Date of Birth</small>
                            <h6><?php echo $row['DateOfBirth']; ?></h6>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-7">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9 mx-auto">
                                <div class="card">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs profile-tab" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Profile</a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Videos</a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home" role="tabpanel">
                                            <div class="card-body">
                                                <div class="col-md-9">
                                                    <div class="card card-body account-right">
                                                        <div class="widget">
                                                            <form action="" method="post">
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                        <fieldset class="form-group">
                                                                            <label><b>Name: </b> <?php echo $row['Name']; ?></label>
                                                                        </fieldset>

                                                                        <fieldset class="form-group">
                                                                            <label><b>Gender: </b>
                                                                                <?php
                                                                                if ($row['Gender'] == 1) {
                                                                                    echo "Female";
                                                                                } else {
                                                                                    echo "Male";
                                                                                }
                                                                                ?>
                                                                            </label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Mobile Number: </b><?php echo $row['ContactNo']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Email: </b><?php echo $row['EmailId']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Address: </b><?php echo $row['Address']; ?></label>
                                                                        </fieldset>
                                                                        <?php
                                                                            include 'connection.php';
                                                                            $city = "select * from tbl_city where CityId='" . $row['CityId'] . "'";
                                                                            $res1 = mysqli_query($con, $city);
                                                                            $cityname = mysqli_fetch_array($res1, MYSQLI_ASSOC);
                                                                            mysqli_close($con);
                                                                        ?>
                                                                        <fieldset class="form-group">
                                                                            <label><b>City: </b><?php echo $cityname['CityName']; ?></label>
                                                                        </fieldset>
                                                                        <?php
                                                                            include 'connection.php';                                                                          
                                                                            $state = "select * from tbl_state where StateId='" . $cityname['StateId'] . "'";
                                                                            $res2 = mysqli_query($con, $state);
                                                                            $statename = mysqli_fetch_array($res2, MYSQLI_ASSOC);
                                                                            mysqli_close($con);
                                                                        ?>
                                                                        <fieldset class="form-group">
                                                                            <label><b>State: </b><?php echo $statename['StateName']; ?></label>
                                                                        </fieldset>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                        <fieldset class="form-group">
                                                                            <label><b>Roles performed: </b><?php echo $row['RolesPerformed']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Achievements: </b><?php echo $row['Achievements']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Skills: </b><?php echo $row['Skills']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Experience: </b><?php echo $row['Experience']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Body Shape: </b><?php echo $row['BodyShape']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Hair color: </b><?php echo $row['HairColor']; ?></label>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label><b>Eye Color: </b><?php echo $row['EyeColor']; ?></label>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="profile" role="tabpanel">
                                            <div class="card-body">
                                                <?php
                                                if ($_SESSION['utype'] == "Artist") {
                                                    ?>
                                                    <a href="AddVideo.php"><h2>Add Videos</h2></a>
                                                    <br>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                $con = mysql_connect('localhost', 'root', '');
                                                mysql_select_db('casting_system');
                                                $qu = "select VideoId,Video,Description from tbl_artist_videos where UserId='".$_SESSION['id']."' and Status=1;";
                                                $resut = mysql_query($qu);
                                                while ($ro = mysql_fetch_assoc($resut)) {
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <video width="320" height="240" controls="controls">
                                                                <source src="Videos/<?php echo $ro['Video']; ?>" type="video/mp4">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                            <br>
                                                            <h6 style="color: black;"><?php echo $ro['Description']; ?></h6>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <?php
                                                            if ($_SESSION['utype'] == "Artist") {
                                                            ?>
                                                            <a href="DeleteVideo.php?uid=<?php echo $ro['VideoId']; ?>"><button class="btn btn-primary py-3 px-4" name="Delete" >Delete</button></a>
                                                                <br>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <?php include 'footer.php'; ?> 
    </body>
</html>