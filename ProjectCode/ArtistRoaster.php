<?php
// to display all the information about artist
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
 else {
    include 'connection.php';
     $selectId="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
     $queryid= mysqli_query($con, $selectId);
     $artistid= mysqli_fetch_array($queryid,MYSQLI_ASSOC);
     mysqli_close($con);
}
?>
<html>
    <head>
        <title>Roster Artist</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">My Roster</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <h4 style="color:#e3c4a8;">Agents Hired</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                          
                                <?php
                                $cnt=0;
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_clients WHERE ArtistId='".$artistid['ArtistId']."'";
                                $execute = mysql_query($query);
                                $count= mysql_num_rows($execute);
                                if($count!=0 && $cnt==0)
                                {
                                    $cnt=1;
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> No of Clients </th>
                                        <th> No of Clients Casted </th>
                                        <th> Experience </th>
                                        <th> Fees </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {
                                        //fetch the senders user type
                                        $selectSQL = "SELECT * from tbl_agents_master where AgentId='".$row['AgentId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);     
                                         echo "<tr><td>{$rowinfo['AgentName']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['EmailId']}</td>"
                                            . "<td>{$rowinfo['NoOfClients']}</td>"
                                            . "<td>{$rowinfo['NoOfClientsCasted']}</td>"
                                            . "<td>{$rowinfo['Experience']}</td>"
                                            . "<td>{$rowinfo['Fees']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'No agents hired yet!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;">Open Jobs Applied</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.ArtistId='".$artistid['ArtistId']."' and tbl_postjob.PostType=1";
                                $execute = mysql_query($query);
                                $count= mysql_num_rows($execute);
                                if($count!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Director Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                        <th> Other </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {
                                        //fetch the senders user type
                                        $selectSQL = "SELECT * from tbl_castingdirector_master where DirectorId='".$row['DirectorId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr><td>{$rowinfo['CompanyName']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['Emailid']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<td>{$row['Other']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'You haven\'t applied for job!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;">Jobs Applied by Agents</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.ArtistId='".$artistid['ArtistId']."' and tbl_postjob.PostType=0";
                                $execute = mysql_query($query);
                                $count = mysql_num_rows($execute);
                                if($count!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Applied by </th>
                                        <th> Director Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                        <th> Other </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {
                                        //fetch the senders user type
                                        $selectname = "SELECT AgentName from tbl_agents_master where AgentId='".$row['AgentId']."'";
                                        $selectResult=mysql_query($selectname);
                                        $rowname = mysql_fetch_assoc($selectResult);
                                        $selectSQL = "SELECT * from tbl_castingdirector_master where DirectorId='".$row['DirectorId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr><td>{$rowname['AgentName']}</td>"
                                            . "<td>{$rowinfo['CompanyName']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['Emailid']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<td>{$row['Other']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'Your agent haven\'t applied for job!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;"> Jobs you are selected in</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $conn = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.ArtistId='".$artistid['ArtistId']."' and tbl_applyjob.SelectStatus=1";
                                $result = mysql_query($query);
                                $counting = mysql_num_rows($result);
                                if($counting!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Director Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                        <th> Other </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($result)) {
                                        //fetch the senders user type
                                        $selectSQL = "SELECT * from tbl_castingdirector_master where DirectorId='".$row['DirectorId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr>"
                                            . "<td>{$rowinfo['CompanyName']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['Emailid']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<td>{$row['Other']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'You haven\'t been selected for job!';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?> 
    </body>
</html>
 