<?php
session_start();
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 </head>
    <body>
          <div class="modal fade" id="successModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                    <a href="index.php"><button type="button" class="close">&times;</button></a>
                  <h4 class="modal-title" style="color:green">Success</h4>
                </div>
                <div class="modal-body">
                  <p>Audition call request sent!</p>
                </div>
                <div class="modal-footer">
                    <a href="index.php"><button type="button" class="btn btn-success">Ok</button></a>
                </div>
              </div>
            </div>
          </div>
        
         <div class="modal fade" id="failModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="color:red">Failed</h4>
                </div>
                <div class="modal-body">
                  <p>Audition call request not sent!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><a href="index.php">Close</a></button>
                </div>
              </div>
            </div>
          </div>
        <!--<script src="js/blurt.js"></script>-->
        <script>
            $('#successModal').modal({backdrop: 'static', keyboard: false}) 
        </script>
    </body>
</html>
<?php
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else {
    list($static, $dynamic) = split(',', $_SESSION['body']);
    $msgbody = "Congratulations! You have been select for the auditions of " . $dynamic;
    include 'connection.php';
    $callNotify = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
    mysqli_stmt_bind_param($callNotify, 'iiss', $_SESSION['id'], $_GET['uid'], $msgbody, date("Y-m-d H:i:s"));
    mysqli_stmt_execute($callNotify);
    
    $dynamic=trim($dynamic);
    
    $selectJobId="select * from tbl_postjob where RoleDescription='".$dynamic."'";
    $getid= mysqli_query($con, $selectJobId);
    $jobid= mysqli_fetch_array($getid,MYSQLI_ASSOC);
    if(!$getid)
    {
        echo mysqli_error($con);
    }
    
    $selectId="select ArtistId from tbl_artist_master where UserId=".$_GET['uid']."";
    $result= mysqli_query($con, $selectId);
    $id= mysqli_fetch_array($result,MYSQLI_ASSOC);
    if(!$result)
    {
        echo mysqli_error($con);
    }
    
    $update="update tbl_applyjob set SelectStatus='1' where ArtistId='".$id['ArtistId']."' and JobId='".$jobid['JobId']."'";
    $status= mysqli_query($con,$update);
    if(!$status)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
    else {
         echo "<script>$('#successModal').modal('show')</script>";
        // echo '<script>location.href="index.php";</script>';
    }
}
?>