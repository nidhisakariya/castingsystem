<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';  
}else {
    include 'connection.php';
    $updatestatus = "UPDATE tbl_notification set Status=1 where RecieverId='" . $_SESSION['id'] . "' and SenderId='".$_GET['uid']."'";
    $res1 = mysqli_query($con, $updatestatus);
}
?>
<html>
    <head>
    <title>Audition Call Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
    <div class="site-wrap">
    <?php 
    include 'header.php';
    ?>    
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">Audition Call</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="site-section">
    <div class="container">
      <div class="row">
        
          <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
              <p>
                  <strong><?php echo $_SESSION['call'];?></strong><br>
                  <br>
                  You are requested to be present at the time of auditions,prepare according to the description provided in the job details and for further more details kindly contact the company.
              </p>
        </div>
        </div>
        <center><a href="index.php"><button class="btn btn-primary py-3 px-4">Ok</button></a></center>
    </div>
  </div>
    <?php include 'footer.php'; ?> 
    </body>
</html>
