<?php
//display all the Agents info to admin 
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="login.php"</script>';
}
else if($_SESSION['utype']!="Admin")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
        <?php
        // include header
        include 'AdminHeader.php';
        include 'database_connection.php';
        include 'connection.php';
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Clients Made By Agents Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Reports</a></li><li class="breadcrumb-item active">Clients Made By Agents</li>
                        </ol>
                    </div>
                    

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>
                </div>
                <div class="row" id="foot">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered m-t-30 table-hover contact-list footable footable-1 footable-paging footable-paging-center breakpoint-sm">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Artist Name</th>
                                                <th>Contact Number</th>
                                                <th>Email Id</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                               
                                                $con = mysql_connect('localhost', 'root', '');
                                                mysql_select_db('casting_system');
                                                //get the status of agent
                                                $query="select * from tbl_clients left join tbl_agents_master on tbl_clients.AgentId=tbl_agents_master.AgentId group by tbl_clients.AgentId";
                                                $result=mysql_query($query);
                                                //fetch one by one
                                               while($row = mysql_fetch_array($result)) {
                                                    //get agents information
                                                    $artist="select * from tbl_artist_master where ArtistId='".$row['ArtistId']."'";
                                                    $artistid=mysql_query($artist);
                                                    $id=mysql_fetch_array($artistid,MYSQLI_ASSOC);
                                                        
                                                                echo "<tr><td>{$row['AgentName']}</td>"
                                                                . "<td>{$id['Name']}</td>"
                                                                . "<td>{$row['ContactNo']}</td>"
                                                                . "<td>{$row['EmailId']}</td>"
                                                                . "</td></tr>\n";
                                                            }
                                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>