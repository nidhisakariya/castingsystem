<html>
    <head>
        <title>AuditionMagic-Contactus</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
    <div class="site-wrap">
        <?php
        session_start();
    if($_SESSION['id'])
        {
            if($_SESSION['utype']=="Artist")
            {
                include 'header.php';
            }
            else if($_SESSION['utype']=="Agent"){
                include 'Agentheader.php';
            }
            else {
                include 'Directorheader.php';
            }
        }
        else {
            include 'header.php';
        }
        ?>
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">Contact Us</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="site-section">
    <div class="container">
      <div class="row">
        
          <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
              <p>
                  <strong>
                      <b>Address:</b>
                  </strong>
                  12/A,Elora Enclaves,
                  <br>S.V.Road,
                  <br>New Delhi-369852
                  <br>
                  <br>
                  <br>
                  <strong>
                      <b>Contact Number:</b>
                  </strong>
                  7485693210, 0241-897453
              </p>
        </div>
        </div>
    </div>
  </div>
    <?php include 'footer.php'; ?> 
    </body>
</html>
