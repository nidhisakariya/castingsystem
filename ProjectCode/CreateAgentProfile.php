<?php
session_start();
if(empty($_SESSION['id']))
    {
        echo '<script>location.href="Login.php";</script>';
    }
    else if($_SESSION['utype']!="Agent")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
    else {
        include 'connection.php';
        $q="SELECT * from tbl_agents_master where UserId='".$_SESSION['id']."'";
        $result=mysqli_query($con,$q);
        $row= mysqli_fetch_array($result,MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);
                
        if($count!=0)
        {
            echo '<script>location.href="AgentProfile.php";</script>';
        }
        mysqli_close($con);
   }
$CastedError = "";
$ExprError = "";
include 'connection.php';
if (isset($_POST['btn_register'])) {
    $cast = test_input($_POST['cilentscasted']);
    $client = test_input($_POST['cilents']);
    $yrs = test_input($_POST['exper']);

    if ($cast > $client) {
        $CastedError = "Invalid no. of casted clients!";
    } else if ($yrs <= "0" || $yrs >= "101") {
        $ExprError = "Invalid years of experience!";
    }
 else {
    $callCreate = mysqli_prepare($con, 'CALL AgentProfile(?, ?, ?, ?, ?, ?, ?, ?)');
    mysqli_stmt_bind_param($callCreate, 'sssiiiii', $_POST['name'], $_POST['contact'], $_POST['email'], $_POST['cilents'], $_POST['cilentscasted'], $_POST['exper'],$_SESSION['id'],$_POST['fee']);
    mysqli_stmt_execute($callCreate);

    echo "<script>location.href='AgentProfile.php';</script>";
 }
}
?>
<html>
    <head>
        <title>Create Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>

    </head>
    <body>
        <div class="site-wrap">
            <?php include 'Agentheader.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="contact-form">  
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="aname" style="color: #e3c4a8;">Name:</label>
                                    <input type="text" name="name" class="form-control" maxlength="25" style="color: black;width: 400px;" placeholder="Enter name" required="required" pattern="^[a-zA-Z ]*$" title="Only alphabets and whiespace are allowed!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact no:</label><br>
                                    <input type="text" name="contact" class="form-control" maxlength="10" pattern="[6-9]{1}[0-9]{9}" title="Only ten digit phone no. allowed which starts with 6,7,8 or 9!" style="color: black;width: 400px;" placeholder="Enter Contact number" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email Id:</label><br>
                                    <input type="email" name="email" maxlength="30" value="<?php echo $_SESSION['uid']?>" class="form-control" style="color: black;width: 400px;" placeholder="Enter Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Please enter valid email id">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="client" style="color: #e3c4a8;">No. of Clients:</label><br>
                                    <input type="text" name="cilents" min="0" pattern="[0-9]{1,}" class="form-control" style="color: black;width: 400px;" placeholder="No. of Clients" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cc" style="color: #e3c4a8;">No. of Clients casted:</label>
                                    <input type="text" name="cilentscasted" class="form-control" pattern="[0-9]{1,}" style="color: black;width: 400px;" placeholder="No. of Clients Casted" required="required">
                                    <label style="color: red"><?php echo $CastedError; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="exp" style="color: #e3c4a8;">Years of Experience:</label><br>
                                    <input type="text" name="exper" min="0" pattern="[0-9]{1,}" class="form-control" style="color: black;width: 400px;" placeholder="Expeirenece in years" required="required">
                                    <label style="color: red"><?php echo $ExprError; ?></label>
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="fees" style="color: #e3c4a8;">Fees charged:</label><br>
                                    <input type="text" name="fee" min="0" pattern="[0-9]{1,}" class="form-control" style="color: black;width: 400px;" placeholder="Fees charged" required="required">
                                    <label style="color: red"><?php echo $ExprError; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Register" name="btn_register" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
            <?php include 'footer.php'; ?> 
            <?php
            function test_input($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
            ?>

            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- Plugins for this page -->
            <!-- ============================================================== -->
            <!-- jQuery file upload -->
            <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script>
            $(function () {
                // Basic
                $('.dropify').dropify();

                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-déposez un fichier ici ou cliquez',
                        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                        remove: 'Supprimer',
                        error: 'Désolé, le fichier trop volumineux'
                    }
                });

                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function (event, element) {
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function (event, element) {
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function (event, element) {
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function (e) {
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
            </script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>
