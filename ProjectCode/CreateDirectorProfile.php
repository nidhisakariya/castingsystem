<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Director")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
else {
    include 'connection.php';
    $q = "SELECT * from tbl_castingdirector_master where UserId='" . $_SESSION['id'] . "'";
    $result = mysqli_query($con, $q);
    if ($result) {
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);

        if ($count != 0) {
            echo '<script>location.href="DirectorProfile.php";</script>';
        }
    }
}
$Serror="";
if (isset($_POST['btn_register'])) {
    if($_POST['state']=="Select State")
    {
        $Serror="Please select a state!";
    }
 else { 
    $callCreate = mysqli_prepare($con, 'CALL DirectorProfile(?, ?, ?, ?, ?, ?, ?, ?)');
    mysqli_stmt_bind_param($callCreate, 'ssissisi', $_POST['companyname'], $_POST['address'], $_POST['city'], $_POST['contact'], $_POST['email'], $_POST['exisyear'],$_POST['about'], $_SESSION['id']);
    mysqli_stmt_execute($callCreate);

    echo "<script>location.href='DirectorProfile.php';</script>";
 }
}
?>
<html>
    <head>
        <title>Create Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>

    </head>
    <body>
        <div class="site-wrap">
<?php include 'Directorheader.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" class="contact-form">  
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cname" style="color: #e3c4a8;">Company Name:</label>
                                    <input type="text" name="companyname" maxlength="20" pattern="^[a-zA-Z ]*$" title="Only alphabets and whitespace are allowed!" class="form-control" style="color: black;width: 400px;" placeholder="Enter company name" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="add" style="color: #e3c4a8;">Address:</label>
                                    <textarea class="form-control" maxlength="50" rows="3" cols="12" name="address" style="color: black;width: 400px;"></textarea>
                                </div>
                            </div>
                            <?php
                            $pdo = new PDO('mysql:host=localhost;dbname=casting_system', 'root', '');
                            $sql = "SELECT StateId,StateName FROM tbl_state";
                            $stmt = $pdo->prepare($sql);
                            $stmt->execute();
                            $state = $stmt->fetchAll();
                            ?>
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="sname" style="color: #e3c4a8;">State:</label>
                                    <select class="form-control" name="state" style="color: black;width: 400px;" onchange="update(this.value)">
                                        <option style="color: #e3c4a8;">Select State</option>
                                            <?php foreach ($state as $name): ?>
                                            <option value="<?= $name['StateId']; ?>"><?= $name['StateName']; ?></option>
                                            <?php endforeach; ?>
                                    </select>
                                    <label style="color:red;"><?php echo $Serror;?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cname" style="color: #e3c4a8;">City:</label>
                                    <select class="form-control" name="city" id="city" style="color: black;width: 400px;">
                                        <option style="color: #e3c4a8;">Select City</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact no:</label><br>
                                    <input type="text" name="contact" maxlength="10" class="form-control" style="color: black;width: 400px;" placeholder="Enter Contact number" required="required" pattern="[6-9]{1}[0-9]{9}" title="Only ten digit phone no. allowed which starts with 6,7,8 or 9!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email Id:</label><br>
                                    <input type="email" name="email" maxlength="30" class="form-control" value="<?php echo $_SESSION['uid']?>" style="color: black;width: 400px;" placeholder="Enter Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Please enter valid email id">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="exs" style="color: #e3c4a8;">Existing Since:</label><br>
                                    <input type="text" name="exisyear" class="form-control" style="color: black;width: 400px;" min="0" pattern="[0-9]{4}" title="Only four digits allowed"  placeholder="Years of Existence" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="abt" style="color: #e3c4a8;">About Comapny:</label>
                                    <textarea class="form-control" rows="3" cols="12" name="about" style="color: black;width: 400px;" pattern="^[a-zA-Z0-9,.!? ]*$" title="Only alphabets,digits , . ? ! are allowed!"></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Register" name="btn_register" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
            <?php include 'footer.php'; ?> 
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- Plugins for this page -->
            <!-- ============================================================== -->
            <!-- jQuery file upload -->
            <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script>
                                        $(function () {
                                            // Basic
                                            $('.dropify').dropify();

                                            // Translated
                                            $('.dropify-fr').dropify({
                                                messages: {
                                                    default: 'Glissez-déposez un fichier ici ou cliquez',
                                                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                                                    remove: 'Supprimer',
                                                    error: 'Désolé, le fichier trop volumineux'
                                                }
                                            });

                                            // Used events
                                            var drEvent = $('#input-file-events').dropify();

                                            drEvent.on('dropify.beforeClear', function (event, element) {
                                                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                                            });

                                            drEvent.on('dropify.afterClear', function (event, element) {
                                                alert('File deleted');
                                            });

                                            drEvent.on('dropify.errors', function (event, element) {
                                                console.log('Has Errors');
                                            });

                                            var drDestroy = $('#input-file-to-destroy').dropify();
                                            drDestroy = drDestroy.data('dropify')
                                            $('#toggleDropify').on('click', function (e) {
                                                e.preventDefault();
                                                if (drDestroy.isDropified()) {
                                                    drDestroy.destroy();
                                                } else {
                                                    drDestroy.init();
                                                }
                                            })
                                        });
            </script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>
