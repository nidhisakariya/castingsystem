<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
else {
    include 'connection.php';
    $q = "SELECT * from tbl_artist_master where UserId='" . $_SESSION['id'] . "'";
    $result = mysqli_query($con, $q);
    if ($result) {
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);

        if ($count != 0) {
            echo '<script>location.href="ArtistProfile.php";</script>';
        }
    }
}
$Serror = $Bserror = $Derror = $Hcerror = $Ecerror = $Perror = $checkbox="";
$val = 2;
if (isset($_POST['btn_register'])) {
    $ans = $_POST['gender'];
    if ($ans == "male") {
        $val = 0;
    } else {
        $val = 1;
    }

    $date1 = date_create($_POST['birth_date']);
    $date2 = date_create(date("Y-m-d"));
    $diff = date_diff($date1, $date2);
    
    if(!empty($_POST['rolescheck']))
    {
        $checkbox = implode(',', $_POST['rolescheck']);
    }
    
    $extension = pathinfo($_FILES["profile"]["name"], PATHINFO_EXTENSION);
    $name = "img" . $_SESSION['id'] . $_FILES["profile"]["name"];
    $targetDir = "ProfilePhotos/";
    $fileName = "$name";
    $targetFilePath = $targetDir . $fileName;
    $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
    $allowTypes = array('jpg', 'png', 'jpeg');
    
    if (!file_exists($_FILES["profile"]["tmp_name"])) {
        $Perror = "This file does not exist!";
    } else if (!in_array($extension, $allowTypes)) {
        $Perror = "Only jpg,jpeg and png files are allowed!";
    } else if ($_POST['state'] == "Select State") {
        $Serror = "Please select valid state!";
    } else if ($diff->format("%R%a") <= 0) {
        $Derror = "Invalid birthdate!";
    } else if ($_POST['bodyshape'] == "Select body shape") {
        $Bserror = "Please select your body shape!";
    } else if ($_POST['haircolor'] == "Select hair color") {
        $Hcerror = "Please select your hair color!";
    } else if ($_POST['eyecolor'] == "Select eye color") {
        $Ecerror = "Please select your eye color!";
    } else {
        $callCreate = mysqli_prepare($con, 'CALL ArtistProfile(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
        mysqli_stmt_bind_param($callCreate, 'sisisssssssisssi',$_POST['nme'], $val, $_POST['address'], $_POST['city'], $_POST['birth_date'], $_POST['bodyshape'], $_POST['haircolor'], $_POST['eyecolor'], $checkbox, $_POST['achievements'],$_POST['skills'], $_POST['expyear'], $fileName, $_POST['email'], $_POST['contact'], $_SESSION['id']);
        mysqli_stmt_execute($callCreate);
        /*if(!$callCreate)
        {
             $print= '<div class="alert alert-danger"><strong>Profile not edited some problem occured!'.mysqli_error($con).'</strong></div>';                   
        }
        else
        {*/
            move_uploaded_file($_FILES["profile"]["tmp_name"], $targetFilePath);
            echo "<script>location.href='ArtistProfile.php';</script>";
        //}
    }
}
?>
<html>
    <head>
        <title>Create Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <div class="site-wrap">
        <?php include 'header.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" class="contact-form" enctype="multipart/form-data">  
                            <div class="col-lg-6 col-md-6">
                                <label class="font-weight-bold" for="name" style="color: #e3c4a8;">Profile photo:</label>
                                <div class="card">
                                    <div class="card-body">
                                        <input type="file" name="profile" id="input-file-now" class="dropify"/>
                                    </div>
                                </div>
                                <label style="color: red"><?php echo $Perror; ?></label>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="name" style="color: #e3c4a8;">Name:</label>
                                    <input type="text" name="nme" maxlength="25" class="form-control" style="color: black;width: 400px;" placeholder="Enter name" required="required" pattern="^[a-zA-Z ]*$" title="Only alphabets and whitespace are allowed!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="lbl_gender" style="color:#e3c4a8;">Gender:</label> 
                                    <br>
                                    <input class="radio" type="radio" name="gender" value="male" style="color: #e3c4a8;width:25px; height:25px;" checked> Male
                                    &nbsp;&nbsp;
                                    <input type="radio" class="radio" name="gender" value="female" style="color: #e3c4a8;width:25px; height:25px;"> Female
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="add" style="color: #e3c4a8;">Address:</label>
                                    <textarea class="form-control" maxlength="50" rows="3" cols="12" name="address" style="color: black;width: 400px;"></textarea>
                                </div>
                            </div>

                            <?php
                            $pdo = new PDO('mysql:host=localhost;dbname=casting_system', 'root', '');
                            $sql = "SELECT StateId,StateName FROM tbl_state";
                            $stmt = $pdo->prepare($sql);
                            $stmt->execute();
                            $state = $stmt->fetchAll();
                            ?>
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="sname" style="color: #e3c4a8;">State:</label>
                                    <select class="form-control" name="state" style="color: black;width: 400px;" onchange="update(this.value)">
                                        <option style="color: #e3c4a8;">Select State</option>
                                            <?php foreach ($state as $name): ?>
                                            <option value="<?= $name['StateId']; ?>"><?= $name['StateName']; ?></option>
                                            <?php endforeach; ?>
                                    </select>
                                    <label style="color:red;"><?php echo $Serror; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cname" style="color: #e3c4a8;">City:</label>
                                    <select class="form-control" name="city" id="city" style="color: black;width: 400px;">
                                        <option style="color: #e3c4a8;">Select City</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="dob" style="color: #e3c4a8;">Date of birth:</label>
                                    <input type="date" class="form-control" name="birth_date" style="color: black;width: 400px;" id="birth_date" required pattern="^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$">
                                    <label style="color:red;"><?php echo $Derror; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email Id:</label><br>
                                    <input type="email" name="email" value="<?php echo $_SESSION['uid']?>" maxlength="30" class="form-control" style="color: black;width: 400px;" placeholder="Enter Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Please enter valid email id">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact no:</label><br>
                                    <input type="text" name="contact" maxlength="10" class="form-control" style="color: black;width: 400px;" placeholder="Enter Contact number" required="required" pattern="[6-9]{1}[0-9]{9}" title="Only ten digit phone no. allowed which starts with 6,7,8 or 9!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="bshape" style="color: #e3c4a8;">Body Shape:</label>
                                    <select class="form-control" name="bodyshape" style="color: black;width: 400px;">
                                        <option style="color: #e3c4a8;">Select body shape</option>
                                        <option>Apple</option>
                                        <option>Banana</option>
                                        <option>Pear</option>
                                        <option>Hourglass</option>
                                        <option>Ectomorph</option>
                                        <option>Endomorph</option>
                                        <option>Mesomorph</option>
                                    </select>
                                    <label style="color:red;"><?php echo $Bserror; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="hcolor" style="color: #e3c4a8;">Hair Color:</label>
                                    <select class="form-control" name="haircolor" style="color: black;width: 400px;">
                                        <option style="color: #e3c4a8;">Select hair color</option>
                                        <option>Black</option>
                                        <option>Blond</option>
                                        <option>Brown</option>
                                        <option>Red</option>
                                        <option>Grey</option>
                                        <option>White</option>
                                    </select>
                                    <label style="color:red;"><?php echo $Hcerror; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="ecolor" style="color: #e3c4a8;">Eye Color:</label>
                                    <select class="form-control" name="eyecolor" style="color: black;width: 400px;">
                                        <option style="color: #e3c4a8;">Select eye color</option>
                                        <option>Amber</option>
                                        <option>Blue</option>
                                        <option>Brown</option>
                                        <option>Grey</option>
                                        <option>Green</option>
                                        <option>Hazel</option>
                                    </select>
                                    <label style="color:red;"><?php echo $Ecerror; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="roles" style="color:#e3c4a8;">Roles performed:</label><br>
                                    <input type="checkbox" name="rolescheck[]" value="Lead Protagonist" class="checkbox"> Lead protagonist &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Lead Antagonist" class="checkbox"> Lead antagonist &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Ingenue" class="checkbox"> Ingénue <br>
                                    <input type="checkbox" name="rolescheck[]" value="Juvenile" class="checkbox"> Juvenile &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Supporting Actor" class="checkbox"> Supporting Actor &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Recurring" class="checkbox"> Recurring <br>
                                    <input type="checkbox" name="rolescheck[]" value="Guest Star" class="checkbox"> Guest star &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Day Player" class="checkbox"> Day player &nbsp;
                                    <input type="checkbox" name="rolescheck[]" value="Photodouble" class="checkbox"> Photodouble
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="skil" style="color: #e3c4a8;">Skills:</label>
                                    <input type="text" name="skills" maxlength="500" class="form-control" style="color: black;width: 400px;" placeholder="Skills" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="achm" style="color: #e3c4a8;">Achievements:</label>
                                    <input type="text" name="achievements" maxlength="500" class="form-control" style="color: black;width: 400px;" placeholder="Awards you have recieved" required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="exp" style="color: #e3c4a8;">Experience:</label><br>
                                    <input type="text" name="expyear" class="form-control" style="color: black;width: 400px;" placeholder="Experience in year" required="required" min="0" pattern="[0-9]{1,2}" title="Only one or twodigits allowed">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Register" name="btn_register" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
<?php include 'footer.php'; ?> 
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- Plugins for this page -->
            <!-- ============================================================== -->
            <!-- jQuery file upload -->
            <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script>
                                        $(function () {
                                            // Basic
                                            $('.dropify').dropify();

                                            // Translated
                                            $('.dropify-fr').dropify({
                                                messages: {
                                                    default: 'Glissez-déposez un fichier ici ou cliquez',
                                                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                                                    remove: 'Supprimer',
                                                    error: 'Désolé, le fichier trop volumineux'
                                                }
                                            });

                                            // Used events
                                            var drEvent = $('#input-file-events').dropify();

                                            drEvent.on('dropify.beforeClear', function (event, element) {
                                                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                                            });

                                            drEvent.on('dropify.afterClear', function (event, element) {
                                                alert('File deleted');
                                            });

                                            drEvent.on('dropify.errors', function (event, element) {
                                                console.log('Has Errors');
                                            });

                                            var drDestroy = $('#input-file-to-destroy').dropify();
                                            drDestroy = drDestroy.data('dropify')
                                            $('#toggleDropify').on('click', function (e) {
                                                e.preventDefault();
                                                if (drDestroy.isDropified()) {
                                                    drDestroy.destroy();
                                                } else {
                                                    drDestroy.init();
                                                }
                                            })
                                        });
            </script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>
