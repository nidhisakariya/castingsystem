<?php
class Director {
    private $host  = 'localhost';
    private $user  = 'root';
    private $password   = "";
    private $database  = "casting_system";   
	private $productTable = 'tbl_castingdirector_master';
	private $dbConnect = false;
    public function __construct(){
        if(!$this->dbConnect){ 
            $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
            if($conn->connect_error){
                die("Error failed to connect to MySQL: " . $conn->connect_error);
            }else{
                $this->dbConnect = $conn;
            }
        }
    }
	private function getData($sqlQuery) {
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		if(!$result){
			die('Error in query: '. mysqli_error());
		}
		$data= array();
		while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
			$data[]=$row;            
		}
		return $data;
	}
	private function getNumRows($sqlQuery) {
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		if(!$result){
			die('Error in query: '. mysqli_error());
		}
		$numRows = mysqli_num_rows($result);
		return $numRows;
	}		
	public function getcname(){
		$sqlQuery = "
			SELECT DISTINCT(CompanyName)
			FROM ".$this->productTable." 
			ORDER BY DirectorId DESC";
        return  $this->getData($sqlQuery);
	}
	public function getcity(){
		$sqlQuery = "
			SELECT DISTINCT(CityId)
			FROM ".$this->productTable." 
                        ORDER BY DirectorId DESC";
        return  $this->getData($sqlQuery);
	}
	public function getexistence(){
		$sqlQuery = "
			SELECT DISTINCT(ExistingSince)
			FROM ".$this->productTable." 
                        ORDER BY DirectorId DESC";
        return  $this->getData($sqlQuery);
	}
	public function searchProducts(){
		$sqlQuery = "SELECT * FROM ".$this->productTable." Where DirectorId<150 ";
		if(isset($_POST["CompanyName"])) {
			$cnameFilterData = implode("','", $_POST["CompanyName"]);
			$sqlQuery .= "
			AND CompanyName IN('".$cnameFilterData."')";
		}
		if(isset($_POST["CityId"])){
			$cityFilterData = implode("','", $_POST["CityId"]);
			$sqlQuery .= "
			AND CityId IN('".$cityFilterData."')";
		}
		if(isset($_POST["ExistingSince"])) {
			$exisFilterData = implode("','", $_POST["ExistingSince"]);
			$sqlQuery .= "
			AND ExistingSince IN('".$exisFilterData."')";
		}
		//$sqlQuery .= " ORDER By price";
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		$totalResult = mysqli_num_rows($result);
		$searchResultHTML = '';
		if($totalResult > 0) {
			while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
				$searchResultHTML .= '
				<div class="col-sm-4 col-lg-3 col-md-3">
				<div style="border:1px solid #ccc; border-radius:5px; padding:16px; margin-bottom:16px; height:250px;">
				<p align="center"><strong><a href="#">'. $row['CompanyName'] .'</a></strong></p>
				<p><strong>Contact No</strong> : '.$row['ContactNo'].' <br/>
                                <strong>Email Id</strong> : '. $row['Emailid'].' <br />
				<strong>Existing Since</strong> : '. $row['ExistingSince'] .' years <br />
				<strong>About Company</strong> : '. $row['AboutCompany'] .' <br />
				</p>
				</div>
				</div>';
			}
		} else {
			$searchResultHTML = '<h3>No talent found.</h3>';
		}
		return $searchResultHTML;	
	}	
}
?>