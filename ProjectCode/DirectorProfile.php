<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Director")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
?>
<html>
    <head>
        <title>CastingMagic-Profile</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
<?php include 'Directorheader.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <!-- Column -->
                <!-- Column -->

                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 mx-auto">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Profile</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="home" role="tabpanel">
                                        <div class="card-body">
                                            <div class="col-md-9">
                                                <div class="card card-body account-right">
                                                    <div class="widget">
                                                        <form action="" method="post">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                                    <?php
                                                                        include 'connection.php';
                                                                        $q = "select * from tbl_castingdirector_master where UserId='" . $_SESSION['id'] . "'";
                                                                        $result = mysqli_query($con, $q);
                                                                        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                                                                        $city = "select * from tbl_city where CityId='" . $row['CityId'] . "'";
                                                                        $res1 = mysqli_query($con, $city);
                                                                        $cityname = mysqli_fetch_array($res1, MYSQLI_ASSOC);

                                                                        $state = "select * from tbl_state where StateId='" . $cityname['StateId'] . "'";
                                                                        $res2 = mysqli_query($con, $state);
                                                                        $statename = mysqli_fetch_array($res2, MYSQLI_ASSOC);
                                                                    ?>
                                                                    <fieldset class="form-group">
                                                                        <label><b>Company Name: </b> <?php echo $row['CompanyName']; ?></label>
                                                                    </fieldset>                                                    
                                                                    <fieldset class="form-group">
                                                                        <label><b>Address: </b><?php echo $row['Address']; ?></label>
                                                                    </fieldset>
                                                                    <fieldset class="form-group">
                                                                        <label><b>City: </b><?php echo $cityname['CityName']; ?></label>
                                                                    </fieldset>
                                                                    <fieldset class="form-group">
                                                                        <label><b>State: </b><?php echo $statename['StateName']; ?></label>
                                                                    </fieldset>
                                                                    <fieldset class="form-group">
                                                                        <label><b>Mobile Number: </b><?php echo $row['ContactNo']; ?></label>
                                                                    </fieldset>
                                                                    <fieldset class="form-group">
                                                                        <label><b>Email: </b><?php echo $row['Emailid']; ?></label>
                                                                    </fieldset>
                                                                    <fieldset class="form-group">
                                                                        <label><b>Existing Since: </b><?php echo $row['ExistingSince']; ?></label>
                                                                    </fieldset>  
                                                                    <fieldset class="form-group">
                                                                        <label><b>About Company: </b><?php echo $row['AboutCompany']; ?></label>
                                                                    </fieldset>  
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<br>
<br>
<br>
<br>
<?php include 'footer.php'; ?> 
</body>
</html>