<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} 
else if($_SESSION['utype']!="Admin")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
        <?php
        include 'AdminHeader.php';
        include 'connection.php';
        include 'database_connection.php';
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Registered Casting Directors Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Reports</a></li><li class="breadcrumb-item active">Registered Casting Directors </li>
                        </ol>
                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>
                </div>
                 <?php 
                        // counting total number of casting directors registered 
                        $q1="SELECT COUNT(*) FROM tbl_users WHERE UserType='Director'";
                        $result1= mysqli_query($con, $q1);
                        $row1 = mysqli_fetch_assoc($result1);
                        $size1 = $row1['COUNT(*)'];
                        
                        $q11="SELECT COUNT(*) FROM tbl_castingdirector_master WHERE ExistingSince<=10";
                        $result11= mysqli_query($con, $q11);
                        $row11 = mysqli_fetch_assoc($result11);
                        $size11= $row11['COUNT(*)'];
                        
                    ?>
                <div class="row" id="foot">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3> Total: <?php echo $size1 ?></h3><br>
                                   <!-- <h3> Number of Companies existing since more than 10 years: <?php echo $size11 ?></h3>-->
                                    <table class="table table-bordered m-t-30 table-hover contact-list footable footable-1 footable-paging footable-paging-center breakpoint-sm">
                                        <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Contact Number</th>
                                                <th>Email Id</th>
                                                <th>Existing Since</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $con = mysql_connect('localhost', 'root', '');
                                            mysql_select_db('casting_system');
                                            $q = "SELECT UserId,AdminStatus FROM tbl_users WHERE Status=1 and UserType='Director'";
                                            $exe = mysql_query($q);
                                            while ($row1 = mysql_fetch_assoc($exe)) {
                                                $selectSQL = "SELECT CompanyName,ContactNo,EmailId,ExistingSince FROM tbl_castingdirector_master WHERE UserId='" . $row1['UserId'] . "'";
                                                if (!( $selectRes = mysql_query($selectSQL) )) {
                                                    echo 'Retrieval of data from Database Failed - #' . mysql_errno() . ': ' . mysql_error();
                                                } else {
                                                    if (mysql_num_rows($selectRes) == 0) {
                                                        
                                                    } else {
                                                        while ($row = mysql_fetch_assoc($selectRes)) {
                                                            echo "<tr><td>{$row['CompanyName']}</td>"
                                                                    . "<td>{$row['ContactNo']}</td>"
                                                                    . "<td>{$row['EmailId']}</td>"
                                                                    . "<td>{$row['ExistingSince']}</td>"
                                                                    
                                                            ?>
                                                           
                                                            <?php }
                                                            "</td></tr>\n";
                                                        }
                                                    }
                                                }
                                            
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>