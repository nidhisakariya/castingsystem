<?php
// to display all the information about artist
session_start();
if(empty($_SESSION['id']))
{
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Director")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
 else {
    include 'connection.php';
     $selectId="select DirectorId from tbl_castingdirector_master where UserId='".$_SESSION['id']."'";
     $queryid= mysqli_query($con, $selectId);
     $dirid= mysqli_fetch_array($queryid,MYSQLI_ASSOC);
     mysqli_close($con);
}
?>
<html>
    <head>
        <title>Roster Director</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'Directorheader.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">My Roster</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <h4 style="color:#e3c4a8;">Job Posted</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                          
                                <?php
                                $cnt=0;
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_postjob WHERE DirectorId='".$dirid['DirectorId']."'";
                                $execute = mysql_query($query);
                                $count= mysql_num_rows($execute);
                                if($count!=0 && $cnt==0)
                                {
                                    $cnt=1;
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Post type </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills needed </th>
                                        <th> Gender </th>
                                        <th> Start Date </th>
                                        <th> End date </th>
                                        <th> Other requirement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {  
                                        if($row['PostType']==1)
                                        {
                                            $type="Open";
                                        }
                                        else
                                        {
                                            $type="Via Agent";
                                        }
                                        if($row['Gender']==1)
                                        {
                                            $gen="Female";
                                        }
                                        else
                                        {
                                            $gen="Male";
                                        }
                                         echo "<tr><td>{$type}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<td>{$gen}</td>"
                                            . "<td>{$row['StartDate']}</td>"
                                            . "<td>{$row['EndDate']}</td>"
                                            . "<td>{$row['Other']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'No jobs posted yet!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;">Open Job Applications Recieved</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.DirectorId='".$dirid['DirectorId']."' and tbl_postjob.PostType=1";
                                $execute = mysql_query($query);
                                $count= mysql_num_rows($execute);
                                if($count!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Artist Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {
                                        //fetch the senders user type
                                        $selectSQL = "SELECT * from tbl_artist_master where ArtistId='".$row['ArtistId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr><td>{$rowinfo['Name']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['EmailId']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'You haven\'t recieved job applications!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;">Jobs Applied by Agents</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.DirectorId='".$dirid['DirectorId']."' and tbl_postjob.PostType=0";
                                $execute = mysql_query($query);
                                $count = mysql_num_rows($execute);
                                if($count!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Applied by </th>
                                        <th> Artist Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($execute)) {
                                        //fetch the senders user type
                                        $selectname = "SELECT AgentName from tbl_agents_master where AgentId='".$row['AgentId']."'";
                                        $selectResult=mysql_query($selectname);
                                        $rowname = mysql_fetch_assoc($selectResult);
                                        $selectSQL = "SELECT * from tbl_artist_master where ArtistId='".$row['ArtistId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr><td>{$rowname['AgentName']}</td>"
                                            . "<td>{$rowinfo['Name']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['EmailId']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'You haven\'t recieved any job application from agent!';
                                }
                                mysql_close($con);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <h4 style="color:#e3c4a8;">Your selections</h4>
                    <div class="row">
                        <table class="table table-bordered table-hover">                         
                                <?php
                                $conn = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                //select all notifications of artist both read and unread, latest first order
                                $query = "SELECT  * FROM tbl_applyjob left join tbl_postjob on tbl_applyjob.JobId=tbl_postjob.JobId WHERE tbl_Applyjob.DirectorId='".$dirid['DirectorId']."' and tbl_applyjob.SelectStatus=1";
                                $result = mysql_query($query);
                                $counting = mysql_num_rows($result);
                                if($counting!=0)
                                {
                                    ?>
                                    <thead>
                                    <tr>
                                        <th> Artist Name </th>
                                        <th> Contact No </th>
                                        <th> Email Id </th>
                                        <th> Role Description </th>
                                        <th> Role Type </th>
                                        <th> Skills </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //fetch data one by one
                                    while ($row = mysql_fetch_assoc($result)) {
                                        //fetch the senders user type
                                        $selectSQL = "SELECT * from tbl_artist_master where ArtistId='".$row['ArtistId']."'";
                                        $selectRes=mysql_query($selectSQL);
                                        $rowinfo = mysql_fetch_assoc($selectRes);  
                                         echo "<tr>"
                                            . "<td>{$rowinfo['Name']}</td>"
                                            . "<td>{$rowinfo['ContactNo']}</td>"
                                            . "<td>{$rowinfo['EmailId']}</td>"
                                            . "<td>{$row['RoleDescription']}</td>"
                                            . "<td>{$row['RoleType']}</td>"
                                            . "<td>{$row['Skills']}</td>"
                                            . "<tr>\n";                                                                       
                                    }
                                }
                                else
                                {
                                    echo 'You haven\'t selected any applicants!';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?> 
    </body>
</html>
 