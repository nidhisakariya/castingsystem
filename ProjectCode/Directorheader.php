<!-- Directors header page--> 
<html>
    <head>
        <meta charset="UTF-8">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Oswald:400,700"> 
        <link rel="stylesheet" href="fonts/icomoon/style.css">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="css/mediaelementplayer.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
        <link rel="stylesheet" href="css/fl-bigmug-line.css">
        <link rel="stylesheet" href="css/aos.css">
        <link rel="stylesheet" href="css/style.css">
        <style>
            #count{
                border-radius: 50%;
                position: relative;
                top: -10px;
                left: -10px;
            }
        </style>
    </head>
    <body>
        <div class="site-navbar mt-4">
            <div class="container py-1">
                <div class="row align-items-center">
                    <div class="col-8 col-md-8 col-lg-4">
                        <h1 class="mb-0"><a href="index.php" class="text-white h2 mb-0"><strong>Casting Magic<span class="text-primary">.</span></strong></a></h1>
                    </div>
                    <div class="col-4 col-md-4 col-lg-8">
                        <nav class="site-navigation text-right text-md-right" role="navigation">

                            <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

                            <ul class="site-menu js-clone-nav d-none d-lg-block">
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li class="has-children">
                                    <a href="#">My Account</a>
                                    <ul class="dropdown arrow-top">
                                        <li><a href="CreateDirectorProfile.php">My profile</a></li>
                                        <li><a href="DirectorRoaster.php">My Roaster</a></li>
                                        <?php
                                        //if alredy logged in show the sign out button
                                        if (!empty($_SESSION['id'])) {
                                            ?>
                                            <li><a href="logout.php"><input type="button" value="Logout" class="btn btn-primary px-4"></a></li>
                                        <?php }
                                        ?>
                                    </ul>
                                </li>
                                <li class="has-children">
                                    <a href="#">Directors</a>
                                    <ul class="dropdown arrow-top">
                                        <li><a href="PostJobOpen.php">Post a job</a></li>
                                        <li><a href="SearchTalent.php">Search Talent</a></li>
                                        <li><a href="HowDirector.php">How it works</a></li>
                                        <?php
                                        //if not logged in show the join button which redirects to registration page
                                        if (empty($_SESSION['id'])) {
                                            ?>
                                            <li><a href="registration.php"><input type="button" value="Join" class="btn btn-primary px-4"></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li><a href="Aboutus.php">About</a></li>
                                <li><a href="ContactUs.php">Contact</a></li>     
                                <?php
                                $count = "";
                                //for notification check if user is logged in
                                if (!empty($_SESSION['id'])) {
                                    //include database connection file
                                    include 'connection.php';
                                    //select the notification which are unread by the logged user
                                    $q = "select * from tbl_notification where RecieverId='" . $_SESSION['id'] . "' and Status=0";
                                    $result = mysqli_query($con, $q);
                                    $count = mysqli_num_rows($result);
                                    //check if their is any unread notification    
                                    if ($count == 0) {
                                        $count = "";
                                    }
                                }
                                ?>
                                <li class="has-children">
                                    <a href="#">
                                    <i class="fl-bigmug-line-notification4"></i>
                                    <span class="badge badge-light" id="count">
                                        <?php 
                                            //display total unread notifications on notification icon
                                            echo $count; 
                                        ?>
                                    </span>
                                    <ul class="dropdown arrow-top">
                                        <?php
                                        // if their are unread messages
                                        if ($count != 0) {
                                            //fetch each notification one by one
                                            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                                                //select the user type of sender
                                                $con = mysql_connect('localhost', 'root', '');
                                                mysql_select_db('casting_system');
                                                $selectType = "select UserType from tbl_users where UserId='" . $row['SenderId'] . "'";
                                                $res1 = mysql_query($selectType);
                                                $rowtype = mysql_fetch_array($res1);
                                                //if user type is agent
                                                if ($rowtype['UserType'] == "Agent") {
                                                    //fetch the name of agent
                                                    $selectName = "select AgentName from tbl_agents_master where UserId='" . $row['SenderId'] . "'";
                                                    $resultName = mysql_query($selectName);
                                                    $rowName = mysql_fetch_array($resultName);
                                                    //display notification in dropdown and redirect to JobApplicationsbyAgent page with senderl's userid
                                                    ?> 
                                                    <li><a href="JobApplicationsbyAgent.php?uid=<?php echo $row['SenderId']; ?>"><strong>AGENT<br><?php echo $rowName['AgentName']; ?></strong><br><?php echo $row['NotificationBody']; ?></a></li>
                                                    <?php
                                                } else {
                                                    //if user type is artist fetch the artist's name
                                                    $selectName = "select Name from tbl_artist_master where UserId='" . $row['SenderId'] . "'";
                                                    $resultName = mysql_query($selectName);
                                                    $rowName = mysql_fetch_array($resultName);
                                                    //display notification in dropdown and redirect to ViewArtistProfileForDirector page with senderl's userid
                                                    ?>
                                                    <li><a href="ViewArtistProfileForDirector.php?uid=<?php echo $row['SenderId']; ?>"><strong>ARTIST<br><?php echo $rowName['Name']; ?></strong><br><?php echo $row['NotificationBody']; ?></a></li>
                                                    <?php
                                                }
                                                echo '<hr>';
                                                //close connection
                                                mysql_close($con);
                                            }
                                        }
                                        //display all notifications tab is user is logged in
                                        if (!empty($_SESSION['id'])) {
                                            include 'connection.php';
                                            //select all tne notification from tbl_notification for the logged in user and display it in AllNotificationsforDirector page
                                            $allnotification = "select * from tbl_notification where RecieverId='" . $_SESSION['id'] . "'";
                                            $getinfonotification = mysqli_query($con, $allnotification);
                                            $cnt = mysqli_num_rows($getinfonotification);
                                            if ($cnt != 0) {
                                                $rows = mysqli_fetch_array($getinfonotification, MYSQLI_ASSOC);
                                                //create session named body to send notification body to display on AllNotifiactions page
                                                $_SESSION['body'] = $rows['NotificationBody'];
                                                ?>       
                                                <li><a href="AllNotificationsforDirector.php?uid=<?php echo $rows['RecieverId']; ?>">All notifications</a></li>
                                                <?php
                                            }
                                            mysqli_close($con);
                                        }
                                        ?>                                           
                                    </ul>
                                    </a>
                                    </li>
                                <?php
                                if(!empty($_SESSION['id']))
                                {
                                        include 'connection.php';
                                        $query="select * from chat_message where to_user_id='".$_SESSION['id']."' and status=1";
                                        $fetch= mysqli_query($con, $query);
                                        $countmessage= mysqli_num_rows($fetch);
                                        if($countmessage==0)
                                        {
                                            $countmessage="";
                                        }
                                ?>
                                <li><a href="chat.php"><i class="fl-bigmug-line-chat55"></i><span class="badge badge-light" id="count">
                                            <?php echo $countmessage; ?></span></a></li>
                                <?php
                                }
                                else {
                                ?>
                                <li><a href="#"><i class="fl-bigmug-line-chat55"></i></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
