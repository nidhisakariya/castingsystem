<?php
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include 'header.php';
        ?>

        <div class="site-mobile-menu">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div> <!-- .site-mobile-menu -->

        <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
             data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                        <h1 class="text-white">Monologues</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
                <?php
                $cnt = 1;
                include 'connection.php';
                $q = "SELECT * FROM tbl_monologues";
                $result = mysqli_query($con, $q);
                while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) {
                    ?>
            <div class="featured-property-half d-flex">
            <?php
                    if ($cnt % 2 == 0) {
                        ?> 
                        
                        <div class = "text" >
                        <?php
                        } else {
                            ?>
                            <div class = "text" style="background-color:#e3c4a8" >
                                <?php
                            }
                            echo "<h2>Title</h2><br>{$row[1]}<br><br><h2>Scene Synopsis</h2><br>{$row[3]}<br><br><h2>Dialogue</h2><br>{$row[4]}";
                            ?>
                        </div>
                        <?php
                        if ($cnt % 2 == 0) {
                            ?>
                            <div class = "text" style="background-color:#e3c4a8" >
                            <?php
                            } else {
                                ?>
                                <div class = "text" >
                                    <?php
                                }
                                echo "<h2>Genre</h2><br>{$row[2]}<br><br><h2>Age Range</h2><br>{$row[5]}<br><br><h2>Writer</h2><br>{$row[6]}<br><br><h2>Date of upload</h2><br>{$row[7]}";
                                ?>     
                            </div>
                                </div>
                            <?php
                            $cnt++;
                        }
                        ?>
                    </div>
                </div>
        </div>
                <?php
                include 'footer.php';
                ?>
                </body>
                </html>