<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else if ($_SESSION['utype'] != "Artist") {
    echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';    
}
else{
        include 'connection.php';
        $id="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
        $select= mysqli_query($con, $id);
        $count= mysqli_num_rows($select);
        mysqli_close($con);
        if($count==0)
        {
            echo '<script>alert("You need to create profile before accessing this page");</script>';
            echo '<script>location.href="CreateProfile.php";</script>';
        }
    
        include 'connection.php';
        $q = "SELECT * from tbl_artist_master where UserId='" . $_SESSION['id'] . "'";
        $result = mysqli_query($con, $q);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);
        mysqli_close($con);
}
    ?>
<?php
                $print="";
                $val = 2;
                if (isset($_POST['btn_update'])) {
                    if ($_POST['gender'] == "male") {
                        $val = 0;
                    } else {
                        $val = 1;
                    }
                    $checkbox = implode(',', $_POST['rolescheck']);
                    include 'connection.php';
                    $update="UPDATE tbl_artist_master SET Name='".$_POST['nme']."', Gender=$val,Address='".$_POST['address']."', CityId='".$_POST['city']."', DateOfBirth='".$_POST['birth_date']."', EmailId='".$_POST['email']."',ContactNo='".$_POST['contact']."', BodyShape='".$_POST['bodyshape']."', HairColor='".$_POST['haircolor']."', EyeColor='".$_POST['eyecolor']."', RolesPerformed='".$checkbox."', Skills='".$_POST['skills']."',Achievements='".$_POST['achievements']."', Experience='".$_POST['expyear']."' WHERE UserId='".$_SESSION['id']."'";                   
                    $result= mysqli_query($con,$update);
                    if($result)
                    {
                        $print= '<div class="alert alert-success"><strong>Profile edited successfully!</strong></div>';
                    }
                    else
                    {
                        $print= '<div class="alert alert-danger"><strong>Profile not edited some problem occured!</strong></div>';
                    }
                }
                ?>
    <html>
        <head>
            <title>Edit Profile</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
            <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
            <link href="css/style.css" rel="stylesheet">
            <link href="css/colors/default.css" id="theme" rel="stylesheet">
            <script type="text/javascript">
                function update(str)
                {
                    var xmlhttp;

                    if (window.XMLHttpRequest)
                    {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else
                    {// code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                        {
                            document.getElementById("city").innerHTML = xmlhttp.responseText;
                        }
                    }

                    xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                    xmlhttp.send();
                }
            </script>

        </head>
        <body>
            <div class="site-wrap">
                <?php include 'header.php'; ?>  
                <div class="site-mobile-menu">
                    <div class="site-mobile-menu-header">
                        <div class="site-mobile-menu-close mt-3">
                            <span class="icon-close2 js-menu-toggle"></span>
                        </div>
                    </div>
                    <div class="site-mobile-menu-body"></div>
                </div> <!-- .site-mobile-menu -->

                <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                     data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                    <div class="container">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                                <h1 class="text-white">Edit Profile</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="site-section">
                    <div class="container">
                        <center>
                            <form action="#" method="POST" class="contact-form" enctype="multipart/form-data">  
                                <?php echo $print; ?>
                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="name" style="color: #e3c4a8;">Name:</label>
                                        <input type="text" name="nme" class="form-control" style="color: black;width: 400px;" value="<?php echo $row['Name'] ?>" required="required">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="lbl_gender" style="color:#e3c4a8;">Gender:</label> 
                                        <br>
                                        <?php
                                        if ($row['Gender'] == "Male") {
                                            ?>
                                            <input class="radio" type="radio" name="gender" value="male" style="width:25px; height:25px;" checked> Male
                                            &nbsp;&nbsp;
                                            <input type="radio" class="radio" name="gender" value="female" style="width:25px; height:25px;"> Female
                                            <?php
                                        } else {
                                            ?>
                                            <input class="radio" type="radio" name="gender" value="male" style="width:25px; height:25px;" > Male
                                            &nbsp;&nbsp;
                                            <input type="radio" class="radio" name="gender" value="female" style="width:25px; height:25px;" checked> Female
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="add" style="color: #e3c4a8;">Address:</label>
                                        <textarea class="form-control" rows="3" cols="12" name="address" style="color: black;width: 400px;"><?php echo $row['Address']; ?></textarea>
                                    </div>
                                </div>

                                <?php
                                $pdo = new PDO('mysql:host=localhost;dbname=casting_system', 'root', '');
                                $sql = "SELECT StateId,StateName FROM tbl_state";
                                $stmt = $pdo->prepare($sql);
                                $stmt->execute();
                                $state = $stmt->fetchAll();
                                ?>
                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="sname" style="color: #e3c4a8;">State:</label>
                                        <select class="form-control" name="state" style="color: black;width: 400px;" onchange="update(this.value)">
                                            <option>Select State</option>
                                            <?php foreach ($state as $name): ?>
                                                <option value="<?= $name['StateId']; ?>"><?= $name['StateName']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="cname" style="color: #e3c4a8;">City:</label>
                                        <select class="form-control" name="city" id="city" style="color: black;width: 400px;">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="dob" style="color: #e3c4a8;">Date of birth:</label>
                                        <input type="date" class="form-control" name="birth_date" style="color: black;width: 400px;" id="birth_date" required pattern="^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$" value="<?php echo $row['DateOfBirth']; ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email Id:</label><br>
                                        <input type="email" name="email" class="form-control" style="color: black;width: 400px;" placeholder="Enter Email" value="<?php echo $row['EmailId']; ?>"required="required">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact no:</label><br>
                                        <input type="text" name="contact" class="form-control" style="color: black;width: 400px;" placeholder="Enter Contact number" required="required" value="<?php echo $row['ContactNo']; ?>">
                                    </div>
                                </div>

                                
                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="bshape" style="color: #e3c4a8;">Body Shape:</label>
                                        <select id="bodyshape" class="form-control" name="bodyshape" style="color: black;width: 400px;">
                                            <option style="color: #e3c4a8;">Select body shape</option>
                                            <option value="Apple"<?=$row['BodyShape'] == 'Apple' ? ' selected="selected"' : '';?>>Apple</option>
                                            <option value="Banana" <?=$row['BodyShape'] == 'Banana' ? ' selected="selected"' : '';?>>Banana</option>
                                            <option value="Pear" <?=$row['BodyShape'] == 'Pear' ? ' selected="selected"' : '';?>>Pear</option>
                                            <option value="Hourglass" <?=$row['BodyShape'] == 'Hourglass' ? ' selected="selected"' : '';?>>Hourglass</option>
                                            <option value="Ectomorph" <?=$row['BodyShape'] == 'Ectomorph' ? ' selected="selected"' : '';?>>Ectomorph</option>
                                            <option value="Endomorph" <?=$row['BodyShape'] == 'Endomorph' ? ' selected="selected"' : '';?>>Endomorph</option>
                                            <option value="Mesomorph"> <?=$row['BodyShape'] == 'Mesomorph' ? ' selected="selected"' : '';?>Mesomorph</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="hcolor" style="color: #e3c4a8;">Hair Color:</label>
                                        <select class="form-control" name="haircolor" style="color: black;width: 400px;">
                                            <option style="color: #e3c4a8;">Select hair color</option>
                                            <option value="Black" <?=$row['HairColor'] == 'Black' ? ' selected="selected"' : '';?>>Black</option>
                                            <option value="Blond" <?=$row['HairColor'] == 'Blond' ? ' selected="selected"' : '';?>>Blond</option>
                                            <option value="Brown" <?=$row['HairColor'] == 'Brown' ? ' selected="selected"' : '';?>>Brown</option>
                                            <option value="Red" <?=$row['HairColor'] == 'Red' ? ' selected="selected"' : '';?>>Red</option>
                                            <option value="Grey" <?=$row['HairColor'] == 'Grey' ? ' selected="selected"' : '';?>>Grey</option>
                                            <option value="White" <?=$row['HairColor'] == 'White' ? ' selected="selected"' : '';?>>White</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="ecolor" style="color: #e3c4a8;">Eye Color:</label>
                                        <select class="form-control" name="eyecolor" style="color: black;width: 400px;">
                                            <option style="color: #e3c4a8;">Select eye color</option>
                                            <option value="Amber" <?=$row['EyeColor'] == 'Amber' ? ' selected="selected"' : '';?>>Amber</option>
                                            <option value="Blue" <?=$row['EyeColor'] == 'Blue' ? ' selected="selected"' : '';?>>Blue</option>
                                            <option value="Brown" <?=$row['EyeColor'] == 'Brown' ? ' selected="selected"' : '';?>>Brown</option>
                                            <option value="Grey" <?=$row['EyeColor'] == 'Grey' ? ' selected="selected"' : '';?>>Grey</option>
                                            <option value="Green" <?=$row['EyeColor'] == 'Green' ? ' selected="selected"' : '';?>>Green</option>
                                            <option value="Hazel" <?=$row['EyeColor'] == 'Hazel' ? ' selected="selected"' : '';?>>Hazel</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="roles" style="color:#e3c4a8;">Roles performed:</label><br>
                                        <input type="checkbox" name="rolescheck[]" value="Lead Protagonist"<?=$row['RolesPerformed'] == 'Lead protagonist' ? ' checked="checked"' : '';?> class="checkbox"> Lead protagonist &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Lead Antagonist" <?=$row['RolesPerformed'] == 'Lead Antagonist' ? ' checked="checked"' : '';?> class="checkbox"> Lead antagonist &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Ingenue" <?=$row['RolesPerformed'] == 'Ingénue' ? ' checked="checked"' : '';?> class="checkbox"> Ingénue <br>
                                        <input type="checkbox" name="rolescheck[]" value="Juvenile" <?=$row['RolesPerformed'] == 'Juvenile' ? ' checked="checked"' : '';?> class="checkbox"> Juvenile &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Supporting Actor" <?=$row['RolesPerformed'] == 'Supporting Actor' ? ' checked="checked"' : '';?> class="checkbox"> Supporting Actor &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Recurring" <?=$row['RolesPerformed'] == 'Recurring' ? ' checked="checked"' : '';?> class="checkbox"> Recurring <br>
                                        <input type="checkbox" name="rolescheck[]" value="Guest Star" <?=$row['RolesPerformed'] == 'Guest star' ? ' checked="checked"' : '';?> class="checkbox"> Guest star &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Day Player" <?=$row['RolesPerformed'] == 'Day player' ? ' checked="checked"' : '';?> class="checkbox"> Day player &nbsp;
                                        <input type="checkbox" name="rolescheck[]" value="Photodouble" <?=$row['RolesPerformed'] == 'Photodouble' ? ' checked="checked"' : '';?> class="checkbox"> Photodouble
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="skil" style="color: #e3c4a8;">Skills:</label>
                                        <input type="text" name="skills" class="form-control" style="color: black;width: 400px;" placeholder="Skills" value="<?php echo $row['Skills']; ?>" required="required">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="achm" style="color: #e3c4a8;">Achievements:</label>
                                        <input type="text" name="achievements" class="form-control" style="color: black;width: 400px;" placeholder="Awards you have recieved" required="required" value="<?php echo $row['Achievements']; ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="exp" style="color: #e3c4a8;">Experience:</label><br>
                                        <input type="text" name="expyear" class="form-control" style="color: black;width: 400px;" placeholder="Experience in year" required="required" value="<?php echo $row['Experience']; ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                        <input type="submit" value="Update" name="btn_update" class="btn btn-primary py-3 px-4">
                                    </div>
                                </div> 
                            </form>   
                        </center>
                    </div>
                </div>
                <?php include 'footer.php'; ?> 
                
                <script src="assets/node_modules/jquery/jquery.min.js"></script>
                <!-- Bootstrap tether Core JavaScript -->
                <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
                <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
                <!-- slimscrollbar scrollbar JavaScript -->
                <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
                <!--Wave Effects -->
                <script src="js/waves.js"></script>
                <!--Menu sidebar -->
                <script src="js/sidebarmenu.js"></script>
                <!--stickey kit -->
                <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
                <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
                <!--Custom JavaScript -->
                <script src="js/custom.min.js"></script>
                <!-- ============================================================== -->
                <!-- Plugins for this page -->
                <!-- ============================================================== -->
                <!-- jQuery file upload -->
                <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
                <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>