<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else {
    include 'connection.php';
        $id="select AgentId from tbl_agents_master where UserId='".$_SESSION['id']."'";
        $select= mysqli_query($con, $id);
        $count= mysqli_num_rows($select);
        mysqli_close($con);
        if($count==0)
        {
            echo '<script>alert("You need to create profile before accessing this page");</script>';
            echo '<script>location.href="CreateAgentProfile.php";</script>';
        }
        
        include 'connection.php';
        $q = "SELECT * from tbl_agents_master where UserId='" . $_SESSION['id'] . "'";
        $result = mysqli_query($con, $q);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = mysqli_num_rows($result);
}
?>
<?php
    $print="";
       if(isset($_POST['btn_update']))
       {
            $update = "UPDATE tbl_agents_master SET AgentName='" . $_POST['anme'] . "', EmailId='" . $_POST['email'] . "',ContactNo='" . $_POST['contact'] . "', NoOfClients='" . $_POST['nc'] . "',NoOfClientsCasted='" . $_POST['ncc'] . "',Experience='" . $_POST['exp'] . "' WHERE UserId='" . $_SESSION['id'] . "'";
                $result = mysqli_query($con, $update);
                if ($result) {
                    $print= '<div class="alert alert-success"><strong>Profile edited successfully!</strong></div>';
                    echo'';
                } else {
                    $print= '<div class="alert alert-danger"><strong>Profile not edited some problem occured!'. mysqli_error($con).'</strong></div>';
                }
       }
?>
<html>
    <head>
        <title>Edit Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'Agentheader.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Edit Profile</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" id="editform" class="contact-form" enctype="multipart/form-data">  
                            <?php echo $print; ?>
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="name" style="color: #e3c4a8;"> Name:</label>
                                    <input type="text" name="anme" id="anme" class="form-control" style="color: black;width: 400px;" value="<?php echo $row['AgentName'] ?>" required="required">
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label
                                        class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact
                                        no:</label><br>
                                    <input type="text"
                                           name="contact" class="form-control" style="color: black;width: 400px;"
                                           placeholder="Enter Contact number" id="contact" required="required" value="<?php echo $row['ContactNo']; ?>">
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label
                                        class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email
                                        Id:</label><br>
                                    <input type="email"
                                           name="email" id="email" class="form-control" style="color: black;width: 400px;"
                                           placeholder="Enter Email" value="<?php echo $row['EmailId'];
                                        ?>"required="required">
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="nc1" style="color: #e3c4a8;">Number of Clients:</label>
                                    <input type="text" name="nc" id="nc" class="form-control" value="<?php echo $row['NoOfClients'] ?>" style="color: black;width: 400px;"  placeholder="Existing since" required="required" >
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="ncc1" style="color: #e3c4a8;">Number of Clients Casted:</label>
                                    <input type="text" name="ncc" id="ncc" class="form-control" value="<?php echo $row['NoOfClientsCasted']; ?>" style="color: black;width: 400px;" placeholder="About company" required="required">
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="exp1" style="color: #e3c4a8;">Years of Experience:</label>
                                    <input type="text" name="exp" id="exp" class="form-control" value="<?php echo $row['Experience']; ?>" style="color: black;width: 400px;" placeholder="About company" required="required">
                                </div>
                            </div>



                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Update" id="update" name="btn_update" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
            <?php include 'footer.php'; ?>
            
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- Plugins for this page -->
            <!-- ============================================================== -->
            <!-- jQuery file upload -->
            <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script>
            $(function () {
                // Basic
                $('.dropify').dropify();

                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-déposez un fichier ici ou cliquez',
                        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                        remove: 'Supprimer',
                        error: 'Désolé, le fichier trop volumineux'
                    }
                });

                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function (event, element) {
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function (event, element) {
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function (event, element) {
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function (e) {
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
            </script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>