<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else {
        include 'connection.php';
        $id="select DirectorId from tbl_castingdirector_master where UserId='".$_SESSION['id']."'";
        $select= mysqli_query($con, $id);
        $count= mysqli_num_rows($select);
        mysqli_close($con);
        if($count==0)
        {
            echo '<script>alert("You need to create profile before accessing this page");</script>';
            echo '<script>location.href="CreateDirectorProfile.php";</script>';
        }
        
    include 'connection.php';
    $q = "SELECT * from tbl_castingdirector_master where UserId='" . $_SESSION['id'] . "'";
    $result = mysqli_query($con, $q);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $count = mysqli_num_rows($result);
}
?>
 <?php
 $print="";
    if(isset($_POST['btn_update']))
    {
                include 'connection.php';
                $update = "UPDATE tbl_castingdirector_master SET
CompanyName='" . $_POST['cnme'] . "',
CityId='" . $_POST['city'] . "',
Emailid='" . $_POST['email'] . "',ContactNo='" . $_POST['contact'] . "',
ExistingSince='" . $_POST['ex'] . "',AboutCompany='" . $_POST['about'] . "' WHERE
UserId='" . $_SESSION['id'] . "'";
                $result = mysqli_query($con, $update);
                if ($result) {
                      $print= '<div class="alert alert-success"><strong>Profile edited successfully!</strong></div>';
                } else {
                    $print= '<div class="alert alert-danger"><strong>Profile not edited some problem occured!</strong></div>';
                } 
    }
            ?>
<html>
    <head>
        <title>CastingMagic-Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>

    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Edit Profile</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" class="contact-form" enctype="multipart/form-data">  
                            <?php echo $print; ?>
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="name" style="color: #e3c4a8;">Company Name:</label>
                                    <input type="text" name="cnme" class="form-control" style="color: black;width: 400px;" value="<?php echo $row['CompanyName'] ?>" required="required">
                                </div>
                            </div>



                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="add" style="color: #e3c4a8;">Address:</label>
                                    <textarea class="form-control" rows="3" cols="12" name="address" style="color: black;width: 400px;"><?php echo $row['Address']; ?></textarea>
                                </div>
                            </div>

                            <?php
                            $pdo = new PDO('mysql:host=localhost;dbname=casting_system', 'root', '');
                            $sql = "SELECT StateId,StateName FROM tbl_state";
                            $stmt = $pdo->prepare($sql);
                            $stmt->execute();
                            $state = $stmt->fetchAll();
                            ?>
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="sname" style="color: #e3c4a8;">State:</label>
                                    <select class="form-control" name="state" style="color: black;width: 400px;" onchange="update(this.value)">
                                        <option>Select State</option>
                                        <?php foreach ($state as $name): ?>
                                            <option value="<?= $name['StateId']; ?>"><?= $name['StateName']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="cname" style="color: #e3c4a8;">City:</label>
                                    <select class="form-control" name="city" id="city" style="color: black;width: 400px;">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label
                                        class="font-weight-bold" for="cnno" style="color: #e3c4a8;">Contact
                                        no:</label><br>
                                    <input type="text"
                                           name="contact" class="form-control" style="color: black;width: 400px;"
                                           placeholder="Enter Contact number" required="required" value="<?php echo $row['ContactNo']; ?>">
                                </div>
                            </div>




                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label
                                        class="font-weight-bold" for="emailid" style="color: #e3c4a8;">Email
                                        Id:</label><br>
                                    <input type="email" name="email" class="form-control" style="color: black;width: 400px;" value="<?php echo $row['Emailid']; ?>"required="required">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="ex1" style="color: #e3c4a8;">Existing Since:</label>
                                    <input type="text" name="ex" class="form-control" value="<?php echo $row['ExistingSince'] ?>" style="color: black;width: 400px;"  placeholder="Existing since" required="required" >
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="about1" style="color: #e3c4a8;">About Company:</label>
                                    <input type="text" name="about" class="form-control" value="<?php echo $row['AboutCompany']; ?>" style="color: black;width: 400px;" placeholder="About company" required="required">
                                </div>
                            </div>



                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Update" name="btn_update" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
            <?php include 'footer.php'; ?>
           
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script
            src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script
            src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script
            src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script
            src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script
            src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!--
============================================================== -->
            <!-- Plugins for this page -->
            <!--
============================================================== -->
            <!-- jQuery file upload -->
            <script
            src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script
            src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>