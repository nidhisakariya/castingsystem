<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Agent")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
    else
    {
        include 'connection.php';
        $id="select AgentId from tbl_agents_master where UserId='".$_SESSION['id']."'";
        $select= mysqli_query($con, $id);
        $count= mysqli_num_rows($select);
        mysqli_close($con);
        if($count==0)
        {
            echo '<script>alert("You need to create profile before accessing this page");</script>';
            echo '<script>location.href="CreateAgentProfile.php";</script>';
        }
    }
?>
<html>
    <head>
        <title>Find job</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'Agentheader.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Find Job</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <div class="row">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Casting Company</th>
                                    <th>Gender</th>
                                    <th>Role Description</th>
                                    <th>Role Type</th>
                                    <th>Skills</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Other</th>
                                    <th></th> 
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $con = mysql_connect('localhost', 'root', '');
                                mysql_select_db('casting_system');
                                $q = "SELECT  * FROM tbl_postjob WHERE EndDate>='".date("Y-m-d")."' and PostType='0'";
                                $exe = mysql_query($q);
                                while ($row1 = mysql_fetch_assoc($exe)) {
                                    $selectSQL = "SELECT tbl_castingdirector_master.UserId,tbl_users.Status,tbl_users.AdminStatus,tbl_castingdirector_master.CompanyName FROM tbl_users inner join tbl_castingdirector_master on tbl_users.UserId=tbl_castingdirector_master.UserId WHERE DirectorId='" . $row1['DirectorId'] . "'";
                                    $selectRes=mysql_query($selectSQL);
                                    $row = mysql_fetch_assoc($selectRes);
                                    if ($row['Status'] == 1 && $row['AdminStatus'] == 1) {
                                        if ($row1['Gender'] == 0) {
                                            $gen = "Male";
                                        } else {
                                            $gen = "Female";
                                        }
                                        echo "<tr><td>{$row['CompanyName']}</td>"
                                        . "<td>{$gen}</td>"
                                        . "<td>{$row1['RoleDescription']}</td>"
                                        . "<td>{$row1['RoleType']}</td>"
                                        . "<td>{$row1['Skills']}</td>"
                                        . "<td>{$row1['StartDate']}</td>"
                                        . "<td>{$row1['EndDate']}</td>"
                                        . "<td>{$row1['Other']}</td>"
                                        . "<td><a href=ViewDirectorProfile.php?did={$row1['DirectorId']}&uid={$row['UserId']}&job={$row1['JobId']}>View Profile</a></td>"
                                        ."<td><a href=SelectArtistforJob.php?did={$row1['DirectorId']}&job={$row1['JobId']}&uid={$row['UserId']}><button class='btn btn-primary py-3 px-4'>Apply For Job</button></a></td></tr>\n";
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
<?php
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}

$no_of_records_per_page = 10;
$offset = ($pageno - 1) * $no_of_records_per_page;

include 'connection.php';
$total_pages_sql = "SELECT COUNT(*) FROM tbl_postjob where PostType='0'";
$result = mysqli_query($con, $total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

$sql = "SELECT * FROM tbl_postjob LIMIT $offset, $no_of_records_per_page";
$res_data = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($res_data)) {
    //here goes the data
}
?>
                <div class="container mt-5 aos-init aos-animate" data-aos="fade-up">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="site-block-27">
                                <ul>
                                    <li><a href="?pageno=1">1</a></li>
                                    <li class="<?php if ($pageno <= 1) {
                    echo 'disabled';
                } ?>">
                                        <a href="<?php if ($pageno <= 1) {
                    echo '#';
                } else {
                    echo "?pageno=" . ($pageno - 1);
                } ?>"><</a>
                                    </li>
                                    <li class="<?php if ($pageno >= $total_pages) {
                    echo 'disabled';
                } ?>">
                                        <a href="<?php if ($pageno >= $total_pages) {
                    echo '#';
                } else {
                    echo "?pageno=" . ($pageno + 1);
                } ?>">></a>
                                    </li>
                                    <li><a href="?pageno=<?php echo $total_pages; ?>">10</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include 'footer.php'; ?> 
    </body>
</html>
