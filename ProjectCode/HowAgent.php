<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            session_start();
    if(!empty($_SESSION['id']))
        {
            if($_SESSION['utype']=="Artist")
            {
                include 'header.php';
            }
            else if($_SESSION['utype']=="Agent"){
                include 'Agentheader.php';
            }
            else {
                include 'Directorheader.php';
            }
        }
        else {
            include 'header.php';
        }
        ?>
        
        <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">How It Works</h1>
        </div>
      </div>
    </div>
  </div>
    
    <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="image" style="background-image: url('images/FindClient1.jpg')"></div>
                    
                    <div class="text">
                        <h2>Find new clients </h2>
                        <p class="mb-5">
                            Search new talent and get contacted by professionals by listing your agency.<br>
                        <h3>Client Management</h3><br>
                        Manage your clients’ applications, images, demo reels and auditions schedules.<br><br>
                        <h3>Latest jobs & auditions</h3><br>
                        We post jobs and auditions for all performers in the entertainment industry.
                        </p>
                        
                    </div>
                </div>
            </div>
    
    
    
        
        <?php 
        include 'footer.php';
        
        ?>
    </body>
</html>