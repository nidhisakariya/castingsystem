<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if(!empty($_SESSION['id']))
        {
            if($_SESSION['utype']=="Artist")
            {
                include 'header.php';
            }
            else if($_SESSION['utype']=="Agent"){
                include 'Agentheader.php';
            }
            else {
                include 'Directorheader.php';
            }
        }
        else {
            include 'header.php';
        }
        ?>       
        <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">How It Works</h1>
        </div>
      </div>
    </div>
  </div>
    
    <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="image" style="background-image: url('images/FindJob1.jpg')"></div>
                    
                    <div class="text">
                        <h2>Find jobs in a flash </h2>
                        <p class="mb-5">
                            Search more jobs than any other platform and get cast fast.<br>
                        <h3>Search made simple</h3><br>
                        Simplify your search of over 6,000 roles using our filtering tool.<br>
                        Search by location, production type, role type and more to find the perfect part.<br><br>
                        <h3>Get instant notification</h3><br>
                        Save your searches to automatically receive alerts when new roles<br>
                        that match your interests are posted.
                        </p>
                        
                    </div>
                </div>
            </div>
    
    
    
        
        <?php 
        include 'footer.php';
        
        ?>
    </body>
</html>