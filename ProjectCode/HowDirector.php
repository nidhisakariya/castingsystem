<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            session_start();
    if(!empty($_SESSION['id']))
        {
            if($_SESSION['utype']=="Artist")
            {
                include 'header.php';
            }
            else if($_SESSION['utype']=="Agent"){
                include 'Agentheader.php';
            }
            else {
                include 'Directorheader.php';
            }
        }
        else {
            include 'header.php';
        }
        ?>
        
        <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">How It Works</h1>
        </div>
      </div>
    </div>
  </div>
    
    <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="image" style="background-image: url('images/D4.jpg')" ></div>
                    
                    <div class="text">
                        <h2>Your gold mine of performers </h2>
                        <p class="mb-5">
                            Browse profiles of actors, models, voiceover talent and other performers.<br>
                            This is where you can check out their headshots, video reels, resumes <br>
                            and more to find the perfect performer you need for your cast.<br>
                        <h3>Post a Job</h3><br>
                        List your casting notices and auditions to immediately broadcast your project<br>
                       to thousands of actors and performers.<br><br>
                        <h3>Casting ahead of the curve</h3><br>
                        When browsing talent in the Talent Database, clicking the "Invite" button will<br>
                        immediately send them an invitation encouraging them to apply.
                        </p>
                        
                    </div>
                </div>
            </div>
    
    
    
        
        <?php 
        include 'footer.php';
        
        ?>
    </body>
</html>