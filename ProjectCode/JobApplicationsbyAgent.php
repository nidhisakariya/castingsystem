<?php
//This page display all the profile's sent by a particular agent to the director for job posted by director through agents
session_start();
//checking for session creation
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else if($_SESSION['utype']!="Director") {  // checking for user type to access this page 
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
    } else {
     // including connectivity file for database    
     include 'connection.php';
     // update the notification status to read
     $updatestatus = "UPDATE tbl_notification set Status=1 where RecieverId='" . $_SESSION['id'] . "' and SenderId='".$_GET['uid']."'";
     $read = mysqli_query($con, $updatestatus);
     if(!$read)
     {
         echo '<script>alert("Status update failed!")</script>';
     }
     
     //selecting agents name and agentid who has sent the applications
     $selectid="select AgentName,AgentId from tbl_agents_master where UserId='".$_GET['uid']."'";
     $run= mysqli_query($con, $selectid);
     $id= mysqli_fetch_array($run,MYSQLI_ASSOC);
     
     // selecting director's directorid 
     $selectdid="select DirectorId from tbl_castingdirector_master where UserId='".$_SESSION['id']."'";
     $get= mysqli_query($con, $selectdid);
     $did= mysqli_fetch_array($get,MYSQLI_ASSOC);
     
     //connection close
     mysqli_close($con);
}
?>
<html>
    <head>
        <title>Job Application</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
        <?php 
            // including header file according to the type of user
            include 'Directorheader.php'; 
        ?>    
        <div class="site-mobile-menu">
          <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
              <span class="icon-close2 js-menu-toggle"></span>
            </div>
          </div>
          <div class="site-mobile-menu-body"></div>
        </div> <!-- .site-mobile-menu -->

        <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
            <div class="container">
              <div class="row align-items-center justify-content-center">
                <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                <h1 class="text-white">Job Application by <?php echo $id['AgentName'];?></h1>
              </div>
            </div>
          </div>
        </div>

        <div class="site-section">
            <div class="container">
                <div class="row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Date Of Birth</th>
                                <th>Email Id</th>
                                <th>Contact Number</th>
                                <th>Profile Photo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            //database connectivity using mysql
                            $con = mysql_connect('localhost', 'root', '');
                            mysql_select_db('casting_system');
                            
                            //query to select artist id and job id information of tbl_applyjob table
                            $query = "SELECT ArtistId,JobId FROM tbl_applyjob WHERE DirectorId='".$did['DirectorId']."' and AgentId='".$id['AgentId']."'";
                            $execute = mysql_query($query);
                            
                            //fetch each row one by one
                            while ($rowids = mysql_fetch_assoc($execute)) {                   
                                //select info of artist's to display
                                $selectSQL = "SELECT Name,Gender,DateOfBirth,ProfilePhoto,EmailId,ContactNo FROM tbl_artist_master WHERE ArtistId='" . $rowids['ArtistId'] . "'";
                                if (!( $selectRes = mysql_query($selectSQL) )) {
                                    echo 'Retrieval of data from Database Failed - #' . mysql_errno() . ': ' . mysql_error();
                                } else {
                                    // fetch each artist's details one by one
                                    while ($row = mysql_fetch_assoc($selectRes)) {
                                    //as per database 0 is saved for male and 1 for female   
                                    if ($row['Gender'] == 0) {
                                        $gen = "Male";
                                    } else {
                                        $gen = "Female";
                                    }
                                    //display user details
                                    echo "<tr><td>{$row['Name']}</td>"
                                    . "<td>{$gen}</td>"
                                    . "<td>{$row['DateOfBirth']}</td>"
                                    . "<td>{$row['EmailId']}</td>"
                                    . "<td>{$row['ContactNo']}</td>"
                                    . "<td><img src=ProfilePhotos/{$row['ProfilePhoto']} class=img-circle alt=Image width=90 height=90 /></td>" 
                                    // for redirectiong to particular artist's all details info page with artist id to display artist's info jobid to kniw for which job and agents user id to get info about agent
                                    . "<td><a href=ViewArtistProfileForCall.php?artid={$rowids['ArtistId']}&job={$rowids['JobId']}&uid={$_GET['uid']}>View Profile</a></td></tr>\n";
                                    }
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 10;
                $offset = ($pageno-1) * $no_of_records_per_page; 

                //including connection for database
                include 'connection.php';
                $total_pages_sql = "SELECT count(*) FROM tbl_applyjob WHERE DirectorId='".$did['DirectorId']."' and AgentId='".$id['AgentId']."'";
                $result = mysqli_query($con,$total_pages_sql);
                $total_rows = mysqli_fetch_array($result)[0];
                $total_pages = ceil($total_rows / $no_of_records_per_page);

                $sql = "SELECT * FROM tbl_applyjob LIMIT $offset, $no_of_records_per_page";
                $res_data = mysqli_query($con,$sql);
                while($row = mysqli_fetch_array($res_data)){
                    //here goes the data
                }
            ?>
            <div class="container mt-5 aos-init aos-animate" data-aos="fade-up">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="site-block-27">
                        <ul>
                            <li><a href="?pageno=1">1</a></li>
                            <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                                <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>"><</a>
                            </li>
                            <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                                <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">></a>
                            </li>
                            <li><a href="?pageno=<?php echo $total_pages; ?>">10</a></li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php
            // include footer file
            include 'footer.php';
        ?> 
    </body>
</html>
