<?php
session_start();
unset($_SESSION["user"]);
unset($_SESSION["otp"]);
unset($_SESSION["email"]);
unset($_SESSION["userid"]);
$print = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include 'connection.php';
    include 'database_connection.php';
    $q = "select UserId,UserType,Status,AdminStatus from tbl_users where UserName='" . $_POST['uname'] . "' and UserPassword='" . md5($_POST['pswd']) . "'";
    $result = mysqli_query($con, $q);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $count = mysqli_num_rows($result);


    if ($count == 1) {
        if ($row['AdminStatus'] == 1 && $row['Status'] == 1) {
            if ($row['UserType'] == "Artist") {
                $_SESSION['utype'] = $row['UserType'];
                $_SESSION['id'] = $row['UserId'];
                $_SESSION['uid'] = $_POST['uname'];
                echo '<script>location.href="index.php"</script>';
            } else if ($row['UserType'] == "Director") {
                $_SESSION['utype'] = $row['UserType'];
                $_SESSION['id'] = $row['UserId'];
                $_SESSION['uid'] = $_POST['uname'];
                echo '<script>location.href="index.php"</script>';
            } else if ($row['UserType'] == "Agent") {
                $_SESSION['utype'] = $row['UserType'];
                $_SESSION['id'] = $row['UserId'];
                $_SESSION['uid'] = $_POST['uname'];
                echo '<script>location.href="index.php"</script>';
            } else {
                $_SESSION['id'] = $row['UserId'];
                $_SESSION['uid'] = $_POST['uname'];
                $_SESSION['utype'] = $row['UserType'];
                echo '<script>location.href="AdminDashboard.php"</script>';
            }
        } else if ($row['AdminStatus'] == 1 && $row['Status'] == 0) {
            $q = "update tbl_users set Status=1 where UserId='" . $_SESSION['id'] . "'";
            $ans = mysqli_query($con, $q);

            if ($ans) {
                if ($row['UserType'] == "Artist") {
                    $_SESSION['utype'] = $row['UserType'];
                    $_SESSION['id'] = $row['UserId'];
                    $_SESSION['uid'] = $_POST['uname'];
                    $sub_query = "
				INSERT INTO login_details 
	     		(user_id) 
	     		VALUES ('".$row['UserId']."')
				";
				$statement = $connect->prepare($sub_query);
				$statement->execute();
				$_SESSION['login_details_id'] = $connect->lastInsertId();
                    echo '<script>location.href="index.php"</script>';
                } else if ($row['UserType'] == "Director") {
                    $_SESSION['utype'] = $row['UserType'];
                    $_SESSION['id'] = $row['UserId'];
                    $_SESSION['uid'] = $_POST['uname'];
                    $sub_query = "
				INSERT INTO login_details 
	     		(user_id) 
	     		VALUES ('".$row['UserId']."')
				";
				$statement = $connect->prepare($sub_query);
				$statement->execute();
				$_SESSION['login_details_id'] = $connect->lastInsertId();
                    echo '<script>location.href="index.php"</script>';
                } else if ($row['UserType'] == "Agent") {
                    $_SESSION['utype'] = $row['UserType'];
                    $_SESSION['id'] = $row['UserId'];
                    $_SESSION['uid'] = $_POST['uname'];
                    $sub_query = "
				INSERT INTO login_details 
	     		(user_id) 
	     		VALUES ('".$row['UserId']."')
				";
				$statement = $connect->prepare($sub_query);
				$statement->execute();
				$_SESSION['login_details_id'] = $connect->lastInsertId();
                    echo '<script>location.href="index.php"</script>';
                } else {
                    $_SESSION['id'] = $row['UserId'];
                    $_SESSION['uid'] = $_POST['uname'];
                    echo '<script>location.href="AdminDashboard.php"</script>';
                }
            }
        } else {
            echo '<script>alert("You are deactivated by admin! Contact him to activate your account!");</script>';
        }
    } else {
        $print = "Your email id or password does not match!";
    }
}
?>
<html>
    <head>
        <title>AuditionMagic-Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="background-image:url('images/login1.jpg');background-size: 100%;background-repeat: repeat-x; ">
        <div class="site-wrap">
            <?php include 'header.php'; ?>
            <br>
            <br>
            <br>
            <div class="site-section ">
                <div class="container">
                    <form action="#" method="POST" class="contact-form">

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="unam" style="color: white;">User Name</label>
                                <input type="text" id="uname" name="uname" class="form-control" style="color: white;width: 400px;" placeholder="User Name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="font-weight-bold" for="psed" style="color: white;">Password</label>
                                <input type="Password" id="pswd" name="pswd" class="form-control" style="color: white;width: 400px;" placeholder="Password">
                                <label style="color:red;"><?php echo $print; ?></label>
                            </div>
                        </div>
                        <br>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" name="submit" value="Login" class="btn btn-primary py-3 px-4">
                            </div>
                        </div> 

                        <div class="row form-group">
                            <div class="col-md-12">
                                <a href="registration.php" style="font-size: 10px;color:white; ">Not registered yet? Register Now!!</a><br>
                                <a href="forgotPassword.php" style="font-size: 10px;color:white; ">Forgot Password?</a>
                            </div>
                        </div>
                    </form>       
                </div>
            </div>
    </body>
</html>
