<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Admin")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
}
$print = "";
if (isset($_POST['btn_submit'])) {
    include 'connection.php';
    $q = "INSERT INTO tbl_monologues(Title,Genre,SceneSynopsis,Dialogue,AgeRange,Writer,DateofUpload) VALUES('" . mysqli_real_escape_string($con,$_POST['title']) . "','" . mysqli_real_escape_string($con,$_POST['genre']) . "','" .  mysqli_real_escape_string($con,$_POST['synopsis']) . "','" . mysqli_real_escape_string($con,$_POST['dialogue']) . "','" . $_POST['agerange'] . "','" . mysqli_real_escape_string($con,$_POST['writer']) . "','" . date("Y-m-d") . "');";
    $result = mysqli_query($con, $q);

    if ($result) {
        $print = '<div class="alert alert-success"><strong>Monologues uploaded successfully!</strong></div>';
    } else {
        $print = '<div class="alert alert-danger"><strong>Some error occurred whille uploading!</strong></div>';
    }
}
?> 
<html lang="en">
    <head>
        <title>Add Monologues</title>                
    </head>
    <body><?php include 'AdminHeader.php'; ?>
        <div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <center><h1 class="text-themecolor">Monologues</h1></center>
                </div>
            </div>
            <?php echo $print; ?>
            <form action="#" method="POST" class="contact-form">   
                <center>                       
                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="title1" style="color: black;">Title</label><br>
                            <input type="text" name="title" maxlength="20" class="form-control" style="color: black;width: 400px;" placeholder="Enter title" required="required" pattern="^[a-zA-Z0,.!? ]*$" title="Only alphabets , . ? ! are allowed!">
                        </div>
                    </div>                   
                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="Genre1" style="color: black;">Genre</label><br>
                            <select class="form-control" style="color: black;width: 400px;" name="genre" >
                                <option style="background-color:burlywood;color: black;">Science fiction</option>
                                <option style="background-color:burlywood;color: black;">Action</option>
                                <option style="background-color:burlywood;color: black;">Comedy</option>
                                <option style="background-color:burlywood;color: black;">Family</option>
                                <option style="background-color:burlywood;color: black;">Thriller</option>
                                <option style="background-color:burlywood;color: black;">Adventure</option>
                                <option style="background-color:burlywood;color: black;">Horror</option>
                                <option style="background-color:burlywood;color: black;">Crime</option>
                                <option style="background-color:burlywood;color: black;">Mystery</option>
                                <option style="background-color:burlywood;color: black;">Drama</option>
                                <option style="background-color:burlywood;color: black;">Romance</option>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="synopsis1" style="color: black;">Scene Synopsis:</label><br>
                            <textarea  type="text" name="synopsis" maxlength="500" rows="5" cols="5" class="form-control" style="color: black;width: 400px;" placeholder="Enter scene synopsis" pattern="^[a-zA-Z,.!? ]*$" title="Only alphabets , . ? ! are allowed!"></textarea>
                        </div>
                    </div

                    <div class="row form-group">
                        <div class="col-md-12 mb-3 mb-md-0">
                            <label class="font-weight-bold" for="dialogue1" style="color: black;">Dialogue:</label><br>
                            <textarea  type="text" name="dialogue" maxlength="700" rows="10" cols="10" class="form-control" style="color: black;width: 400px;" placeholder="Enter dialogue" required="required" pattern="^[a-zA-Z,.!? ]*$" title="Only alphabets , . ? ! are allowed!"></textarea><br>
                        </div>


                        <center><div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="agerange1" style="color: black;">Age Range:</label><br>
                                    <input type="text" name="agerange" class="form-control" style="color: black;width: 400px;" placeholder="Enter age range" required="required" pattern="[0-9][0/9]-[0-9][0-9]" title="Valid age range consists of two ages separated by hyphon!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="writer1" style="color: black;">Writer:</label><br>
                                    <input type="text" name="writer" class="form-control" style="color: black;width: 400px;" placeholder="Enter name of writer" pattern="^[a-zA-Z ]*$" title="Only alphabets and whitespace are allowed!">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="submit" value="Add" name="btn_submit" class="btn btn-primary py-3 px-4">
                                </div>
                            </div></center></div>
                </center>
            </form>
        </div>
    </body>
</html>