<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Director")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
}
else {
        include 'connection.php';
        $id="select DirectorId from tbl_castingdirector_master where UserId='".$_SESSION['id']."'";
        $select= mysqli_query($con, $id);
        $count= mysqli_num_rows($select);
        mysqli_close($con);
        if($count==0)
        {
            echo '<script>alert("You need to create profile before accessing this page");</script>';
            echo '<script>location.href="CreateDirectorProfile.php";</script>';
        }
        
    include 'connection.php';
    $q = "SELECT DirectorId from tbl_castingdirector_master where UserId='" . $_SESSION['id'] . "'";
    $result = mysqli_query($con, $q);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if ($result) {
        $count = mysqli_num_rows($result);

        if ($count == 0) {
            echo '<script>location.href="CreateDirectorProfile.php";</script>';
        }
    }
}
$Rtype = $Sdate = $Edate = $print=$Postype="";
if (isset($_POST['btn_post'])) {
    $date1 = date_create($_POST['startdate']);
    $date2 = date_create(date("Y-m-d"));
    $diff = date_diff($date1, $date2);

    $date3 = date_create($_POST['enddate']);
    $diff1 = date_diff($date1, $date3);


    if ($_POST['roletype'] == "Select Role Type") {
        $Rtype = "Please select valid role type!";
    } else if ($diff->format("%R%a") > 0) {
        $Sdate = "Start date cannot be before today!";
    } else if (($diff1->format("%R%a")) <= 0) {
        $Edate = "End date cannot be before or same as Start date!";
    } else if($_POST['posttype'] == "Select Post Type")
    {
        $Postype= "Please select a type to post job!";
    }
    else {
        if($_POST['posttype'] == "Open")
        {
            $PostType=1;
        }
        else
        {
            $PostType=0;
        }

        if ($_POST['gender'] == "male") {
            $gender = 0;
        } else {
            $gender = 1;
        }

        $q = "INSERT INTO tbl_PostJob(DirectorId,PostType,Gender,RoleDescription,RoleType,Skills,StartDate,EndDate,Other)"
                . "VALUES('" . $row['DirectorId'] . "', $PostType , $gender ,'" . $_POST['roledec'] . "','" . $_POST['roletype'] . "','" . $_POST['skills'] . "','" . $_POST['startdate'] . "','" . $_POST['enddate'] . "','" .$_POST['other'] . "')";

        $result = mysqli_query($con, $q);

        if ($result) {
            $print='<div class="alert alert-success"><strong>Job posted successfully!</strong></div>';
        } else {
            $print='<div class="alert alert-danger"><strong>Sorry! Some problem occurred!"'.mysqli_error($con).'"</strong></div>';
        }
    }
}
?>
<html>
    <head>
        <title>Post Job</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
        <?php include 'Directorheader.php'; ?>
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Post Job</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <?php echo $print;?>
                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="contact-form"> 
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="posttype1" style="color: #e3c4a8;">Post Type:</label>
                                    <select class="form-control" name="posttype" style="color: black;width: 400px;" required>
                                        <option style="color: #e3c4a8;">Select Post Type</option>
                                        <option value="Open">Open</option>
                                        <option value="ViaAgent">Via Agent</option>
                                    </select>
                                    <label style="color: red"><?php echo $Postype; ?></label>
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="lbl_gender" style="color:#e3c4a8;">Gender:</label>
                                    <br>
                                    <input class="radio" type="radio" name="gender" value="male" style="width:25px; height:25px;" checked> Male
                                    &nbsp;&nbsp;
                                    <input type="radio" class="radio" name="gender" value="female" style="width:25px; height:25px;"> Female
                                </div>
                            </div><br>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="roleDesc1" style="color:#e3c4a8;">Role Description:</label><br>
                                    <textarea  type="text" maxlength="500" name="roledec" rows="10" cols="10" class="form-control" style="color: black;width: 400px;" placeholder="Description of the role" required="required" pattern="^[a-zA-Z0-9,.!? ]*$" title="Only alphabets,digits , . ? ! are allowed!"></textarea><br>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="roletype1" style="color: #e3c4a8;">Role Type:</label>
                                    <select class="form-control" name="roletype" style="color: black;width: 400px;" required>
                                        <option style="color: #e3c4a8;">Select Role Type</option>
                                        <option>Lead protagonist</option>
                                        <option>Ingénue</option>
                                        <option>Juvenile</option>
                                        <option>Supporting Actor</option>
                                        <option>Recurring</option>
                                        <option>Guest star</option>
                                        <option>Day player</option>
                                        <option>Photodouble</option>
                                    </select>
                                    <label style="color: red"><?php echo $Rtype; ?></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="skills1" style="color:#e3c4a8;">Required Skills:</label><br>
                                    <textarea  type="text" maxlength="30" name="skills" rows="3" cols="5" class="form-control" style="color: black;width: 400px;" placeholder="Description of the role" pattern="^[a-zA-Z0-9,.!? ]*$" title="Only alphabets,digits , . ? ! are allowed!"></textarea><br>
                                </div></div><br>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="sd1" style="color: #e3c4a8;">Start Date</label><br>
                                    <input type="date" name="startdate"  class="form-control" style="color: black;width: 400px;" placeholder="Enter the start date" required="required">
                                    <label style="color: red"><?php echo $Sdate; ?></label>
                                </div>
                            </div><br>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="ed1" style="color: #e3c4a8;">End Date</label><br>
                                    <input type="date" name="enddate"  class="form-control" style="color: black;width: 400px;" placeholder="Enter the end date" required="required">
                                    <label style="color: red"><?php echo $Edate; ?></label>
                                </div>
                            </div><br>

                            <div class="row form-group">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label class="font-weight-bold" for="other1" style="color:#e3c4a8;">Other Specifications:</label><br>
                                    <textarea  type="text" maxlength="30" name="other" rows="3" cols="5" class="form-control" style="color: black;width: 400px;" placeholder="Enter other specifications" pattern="^[a-zA-Z0-9,.!? ]*$" title="Only alphabets,digits , . ? ! are allowed!"></textarea><br>
                                </div></div><br>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="submit" value="Post" name="btn_post" class="btn btn-primary py-3 px-4">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp; 
                                </div>
                            </div>
                        </form>
                    </center>
                </div>
            </div>
<?php include 'footer.php'; ?>
<?php

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
    </body>
</html>