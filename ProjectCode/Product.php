<?php
class Product {
    private $host  = 'localhost';
    private $user  = 'root';
    private $password   = "";
    private $database  = "casting_system";   
	private $productTable = 'tbl_artist_master';
	private $dbConnect = false;
    public function __construct(){
        if(!$this->dbConnect){ 
            $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
            if($conn->connect_error){
                die("Error failed to connect to MySQL: " . $conn->connect_error);
            }else{
                $this->dbConnect = $conn;
            }
        }
    }
	private function getData($sqlQuery) {
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		if(!$result){
			die('Error in query: '. mysqli_error());
		}
		$data= array();
		while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
			$data[]=$row;            
		}
		return $data;
	}
	private function getNumRows($sqlQuery) {
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		if(!$result){
			die('Error in query: '. mysqli_error());
		}
		$numRows = mysqli_num_rows($result);
		return $numRows;
	}		
	public function getGender(){
		$sqlQuery = "
			SELECT DISTINCT(Gender)
			FROM ".$this->productTable." 
			ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
	public function getBodyshape(){
		$sqlQuery = "
			SELECT DISTINCT(BodyShape)
			FROM ".$this->productTable." 
                        ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
	public function gethcolor(){
		$sqlQuery = "
			SELECT DISTINCT(HairColor)
			FROM ".$this->productTable." 
                        ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
        public function getecolor(){
		$sqlQuery = "
			SELECT DISTINCT(EyeColor)
			FROM ".$this->productTable." 
                        ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
        public function getskills(){
		$sqlQuery = "
			SELECT DISTINCT(Skills)
			FROM ".$this->productTable." 
                        ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
        public function getroles(){
		$sqlQuery = "
			SELECT DISTINCT(RolesPerformed)
			FROM ".$this->productTable." 
                        ORDER BY ArtistId DESC";
        return  $this->getData($sqlQuery);
	}
	public function searchProducts(){
		$sqlQuery = "SELECT * FROM ".$this->productTable." Where Gender in (1,0) ";
		if(isset($_POST["Gender"])) {
			$genderFilterData = implode("','", $_POST["Gender"]);
			$sqlQuery .= "
			AND Gender IN('".$genderFilterData."')";
		}
		if(isset($_POST["BodyShape"])){
			$bshapeFilterData = implode("','", $_POST["BodyShape"]);
			$sqlQuery .= "
			AND BodyShape IN('".$bshapeFilterData."')";
		}
		if(isset($_POST["HairColor"])) {
			$hcolorFilterData = implode("','", $_POST["HairColor"]);
			$sqlQuery .= "
			AND HairColor IN('".$hcolorFilterData."')";
		}
                if(isset($_POST["EyeColor"])) {
			$ecolorFilterData = implode("','", $_POST["EyeColor"]);
			$sqlQuery .= "
			AND EyeColor IN('".$ecolorFilterData."')";
		}
                if(isset($_POST["Skills"])) {
			$skillFilterData = implode("','", $_POST["Skills"]);
			$sqlQuery .= "
			AND Skills IN('".$skillFilterData."')";
		}
		//$sqlQuery .= " ORDER By price";
		$result = mysqli_query($this->dbConnect, $sqlQuery);
		$totalResult = mysqli_num_rows($result);
		$searchResultHTML = '';
		if($totalResult > 0) {
			while ($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
                                if($row['Gender']=="0")
                                {
                                    $gen="Male";
                                }
                                else
                                {
                                    $gen="Female";
                                }
                                $searchResultHTML .= '
				<div class="col-sm-4 col-lg-3 col-md-3">
				<div style="border:1px solid #ccc; border-radius:5px; padding:16px; margin-bottom:16px; height:500px;">
				<img src="ProfilePhotos/'. $row['ProfilePhoto'] .'" alt="" class="img-responsive">
				<p align="center"><strong><a href="#">'. $row['Name'] .'</a></strong></p>
				<p><strong>Gender</strong> :'. $gen .'<br/>
                                <strong>Date Of Birth</strong> : '. $row['DateOfBirth'].' <br />
				<strong>Body Shape</strong> : '. $row['BodyShape'] .' <br />
				<strong>Hair Color</strong> : '. $row['HairColor'] .' <br />
				<strong>Eye Color</strong> : '. $row['EyeColor'] .'  <br/>
                                <strong>Roles Performed</strong> : '.$row['RolesPerformed'].'<br/>
                                <strong>Achievements</strong> : '.$row['Achievements'].'<br/>
                                <strong>Skills</strong> : '.$row['Skills'].'<br/>
                                <strong>Experience</strong> : '.$row['Experience'].'<br/></p>
				</div>
				</div>';
			}
		} else {
			$searchResultHTML = '<h3>No talent found.</h3>';
		}
		return $searchResultHTML;	
	}	
}
?>