<?php session_start();
if($_SESSION['utype']!="Admin")
{
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include 'AdminHeader.php';
        include 'database_connection.php';
        include 'connection.php';
        ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Registration Fee Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Reports</a></li><li class="breadcrumb-item active">Registration Fee</li>
                        </ol>
                    </div>
                    <?php 
                       
                       
                       // counting total number of artist's registered 
                       $q="SELECT COUNT(*) FROM tbl_users WHERE UserType='Artist'";
                       $result= mysqli_query($con, $q);
                       $row = mysqli_fetch_assoc($result);
                       $size = $row['COUNT(*)'];
                       ?>
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>
                </div>
                <div class="row" id="foot">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3>Total Earnings: Rs.<?php echo $size * 5000?></h3>
                                    <table class="table table-bordered m-t-30 table-hover contact-list footable footable-1 footable-paging footable-paging-center breakpoint-sm">
                                        <thead>
                                            <tr>
                                                <th>Artist Name</th>
                                                <th>Subscription Paid</th>
                                                <th>Email Id</th>
                                                <th>Contact Number</th>
                                                <th>Profile Photo</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $con = mysql_connect('localhost', 'root', '');
                                            mysql_select_db('casting_system');
                                            $q = "SELECT UserId,AdminStatus FROM tbl_users WHERE Status=1 and UserType='Artist'";
                                            $exe = mysql_query($q);
                                            while ($row1 = mysql_fetch_assoc($exe)) {
                                                $selectSQL = "SELECT Name,Gender,DateOfBirth,ProfilePhoto,EmailId,ContactNo FROM tbl_artist_master WHERE UserId='" . $row1['UserId'] . "'";
                                                if (!( $selectRes = mysql_query($selectSQL) )) {
                                                    echo 'Retrieval of data from Database Failed - #' . mysql_errno() . ': ' . mysql_error();
                                                } else {
                                                    if (mysql_num_rows($selectRes) == 0) {
                                                        
                                                    } else {
                                                        while ($row = mysql_fetch_assoc($selectRes)) {
                                                            if ($row['Gender'] == 0) {
                                                                $gen = "Male";
                                                            } else {
                                                                $gen = "Female";
                                                            }
                                                            echo "<tr><td>{$row['Name']}</td>"
                                                            . "<td>5000</td>"
                                                            
                                                            . "<td>{$row['EmailId']}</td>"
                                                            . "<td>{$row['ContactNo']}</td>"
                                                            . "<td><img src=ProfilePhotos/{$row['ProfilePhoto']} class=img-circle alt=Image width=50 height=50 /></td>"
                                                            
                                                            ?>
                                                           
                                                            <?php }
                                                            "</td></tr>\n";
                                                        }
                                                    }
                                                }
                                            
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>