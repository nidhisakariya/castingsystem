<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<title>Search Director</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="js/searchdir.js"></script>
<body>
<div class="site-wrap">
            <?php include 'header.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Search Director</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-section">
        <div class="container">		
	
        <?php
	include 'Director.php';
	$product = new Director();	
	?>	
	<div class="row">
	<div class="col-md-3">                   
		<div class="list-group">
			<h3>Companies</h3>
			<?php
			$CompanyName = $product->getcname();
			foreach($CompanyName as $CompanyNameDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail CompanyName" value="<?php echo $CompanyNameDetails['CompanyName']; ?>"  > <?php echo $CompanyNameDetails['CompanyName']; ?> </label>
			</div>
                        <br>
			<?php
			}
			?> 
                </div>
                <div class="list-group">
			<h3>City</h3>
			<?php			
			$CityId = $product->getcity();
			foreach($CityId as $CityIdDetails){
                            include 'connection.php';
                            $query="select CityName from tbl_city where CityId='".$CityIdDetails['CityId']."'";
                            $result= mysqli_query($con, $query);
                            $rowcity= mysqli_fetch_array($result,MYSQLI_ASSOC);
                            mysqli_close($con);
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail CityId" value="<?php echo $CityIdDetails['CityId']; ?>" > <?php echo $rowcity['CityName']; ?> </label>
			</div>
                        <br>
			<?php    
			}
			?>
		</div> 
                <div class="list-group">
			<h3>Existence</h3>
			<?php
			$ExistingSince = $product->getexistence();
			foreach($ExistingSince as $ExistingSinceDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail ExistingSince" value="<?php echo $ExistingSinceDetails['ExistingSince']; ?>"  > <?php echo $ExistingSinceDetails['ExistingSince']; ?> years </label>
			</div>
                        <br>
			<?php
			}
			?> 
                </div>
	</div>
	<div class="col-md-9">
	 <br/>
		<div class="row searchResult">
		</div>
	</div>
    </div>
</div>	
                </div>
</div>
    <?php include 'footer.php';?>
</body>
</html>