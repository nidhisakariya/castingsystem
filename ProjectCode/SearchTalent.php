<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<title>Search Talent</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="js/search.js"></script>
<body>
<div class="site-wrap">
            <?php include 'Directorheader.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Search Talent</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-section">
        <div class="container">		
	
        <?php
	include 'Product.php';
	$product = new Product();	
	?>	
	<div class="row">
	<div class="col-md-3">                    
		<div class="list-group">
			<h3>Gender</h3>
			<div class="brandSection">
				<?php
				$Gender = $product->getGender();
				foreach($Gender as $genderDetails){	
                                    if($genderDetails["Gender"]==1)
                                    {
                                        $val="Female";
                                    }
                                    else {
                                        $val="Male";
                                    }
				?>
				<div class="list-group-item checkbox">
                                    <label><input type="checkbox" class="productDetail Gender" value="<?php echo $genderDetails["Gender"]?>"  > <?php echo $val;?> </label>
				</div>
                            <br>
				<?php }	?>
			</div>
		</div>
		<div class="list-group">
			<h3>Body Shape</h3>
			<?php			
			$BodyShape = $product->getBodyshape();
			foreach($BodyShape as $bshapeDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail BodyShape" value="<?php echo $bshapeDetails['BodyShape']; ?>" > <?php echo $bshapeDetails['BodyShape']; ?> </label>
			</div>
                        <br>
			<?php    
			}
			?>
		</div>    
		<div class="list-group">
			<h3>Hair Color</h3>
			<?php
			$HairColor = $product->gethcolor();
			foreach($HairColor as $hcolorDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail HairColor" value="<?php echo $hcolorDetails['HairColor']; ?>"  > <?php echo $hcolorDetails['HairColor']; ?> </label>
			</div>
                        <br>
			<?php
			}
			?> 
		</div>
                <div class="list-group">
			<h3>Eye Color</h3>
			<?php
			$EyeColor = $product->getecolor();
			foreach($EyeColor as $ecolorDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail EyeColor" value="<?php echo $ecolorDetails['EyeColor']; ?>"  > <?php echo $ecolorDetails['EyeColor']; ?> </label>
			</div>
                        <br>
			<?php
			}
			?> 
		</div>
                <div class="list-group">
			<h3>Skills</h3>
			<?php
			$Skills = $product->getskills();
			foreach($Skills as $skillDetails){	
			?>
			<div class="list-group-item checkbox">
                            <label><input type="checkbox" class="productDetail Skills" value="<?php echo $skillDetails['Skills']; ?>"  > <?php echo $skillDetails['Skills']; ?> </label>
			</div>
                        <br>
			<?php
			}
			?> 
		</div>
	</div>
	<div class="col-md-9">
	 <br/>
		<div class="row searchResult">
		</div>
	</div>
    </div>
</div>	
                </div>
</div>
    <?php include 'footer.php';?>
</body>
</html>