<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
} else if ($_SESSION['utype'] != "Agent") {
    echo '<script>alert("You are not allowed to access this page");</script>';
    echo '<script>location.href="index.php"</script>';
} else {
    include 'connection.php';
    $selectid = "select AgentId from tbl_agents_master where UserId='" . $_SESSION['id'] . "'";
    $run = mysqli_query($con, $selectid);
    $id = mysqli_fetch_array($run, MYSQLI_ASSOC);
    mysqli_close($con);
}
?>
<html>
    <head>
        <title>Search Artist</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
<?php include 'Agentheader.php'; ?>    
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Select Client for Job Application</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <div class="row">
                    <?php
                    $con = mysql_connect('localhost', 'root', '');
                    mysql_select_db('casting_system');
                    $q = "SELECT ArtistId FROM tbl_clients WHERE AgentId='" . $id['AgentId'] . "'";
                    $exe = mysql_query($q);
                    if(mysql_num_rows($exe)==0)
                    {
                         echo "You don't have any clients yet!";
                    }
                    else
                    {
                        echo '<table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Gender</th>
                                                <th>Date Of Birth</th>
                                                <th>Email Id</th>
                                                <th>Contact Number</th>
                                                <th>Profile Photo</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                    while ($row1 = mysql_fetch_assoc($exe)) {
                        $selectSQL = "SELECT Name,Gender,DateOfBirth,ProfilePhoto,EmailId,ContactNo FROM tbl_artist_master WHERE ArtistId='" . $row1['ArtistId'] . "'";
                        if (!( $selectRes = mysql_query($selectSQL) )) {
                            echo 'Retrieval of data from Database Failed - #' . mysql_errno() . ': ' . mysql_error();
                        } else {
                            if (mysql_num_rows($selectRes) == 0) {
                               
                            } else {
                                    while ($row = mysql_fetch_assoc($selectRes)) {
                                        if ($row['Gender'] == 0) {
                                            $gen = "Male";
                                        } else {
                                            $gen = "Female";
                                        }
                                        echo "<tr><td>{$row['Name']}</td>"
                                        . "<td>{$gen}</td>"
                                        . "<td>{$row['DateOfBirth']}</td>"
                                        . "<td>{$row['EmailId']}</td>"
                                        . "<td>{$row['ContactNo']}</td>"
                                        . "<td><img src=ProfilePhotos/{$row['ProfilePhoto']} class=img-circle alt=Image width=90 height=90 /></td>"
                                        . "<td><a href=ApplyforJobViaAgent.php?artid={$row1['ArtistId']}&did={$_GET['did']}&job={$_GET['job']}&uid={$_GET['uid']}>Apply for job</a></td></tr>\n";
                                    }
                                }
                            }
                        }
                    }
                        ?>
                        </tbody>
                        </table>
                    </div>
                </div>
<?php
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}

$no_of_records_per_page = 10;
$offset = ($pageno - 1) * $no_of_records_per_page;

include 'connection.php';
$total_pages_sql = "SELECT COUNT(*) FROM tbl_artist_master";
$result = mysqli_query($con, $total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

$sql = "SELECT * FROM tbl_artist_master LIMIT $offset, $no_of_records_per_page";
$res_data = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($res_data)) {
    //here goes the data
}
?>
                <div class="container mt-5 aos-init aos-animate" data-aos="fade-up">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="site-block-27">
                                <ul>
                                    <li><a href="?pageno=1">1</a></li>
                                    <li class="<?php if ($pageno <= 1) {
                    echo 'disabled';
                } ?>">
                                        <a href="<?php if ($pageno <= 1) {
                    echo '#';
                } else {
                    echo "?pageno=" . ($pageno - 1);
                } ?>"><</a>
                                    </li>
                                    <li class="<?php if ($pageno >= $total_pages) {
                    echo 'disabled';
                } ?>">
                                        <a href="<?php if ($pageno >= $total_pages) {
                    echo '#';
                } else {
                    echo "?pageno=" . ($pageno + 1);
                } ?>">></a>
                                    </li>
                                    <li><a href="?pageno=<?php echo $total_pages; ?>">10</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include 'footer.php'; ?> 
    </body>
</html>
