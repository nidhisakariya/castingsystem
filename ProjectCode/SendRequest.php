<?php
session_start();
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 </head>
    <body>
    <div class="modal fade" id="successModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                    <a href="FindClients.php"><button type="button" class="close">&times;</button></a>
                  <h4 class="modal-title" style="color:green">Success</h4>
                </div>
                <div class="modal-body">
                  <p>Hiring requuest sent!</p>
                </div>
                <div class="modal-footer">
                    <a href="FindClients.php"><button type="button" class="btn btn-success">Ok</button></a>
                </div>
              </div>
            </div>
          </div>
        
         <div class="modal fade" id="failModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="color:red">Failed</h4>
                </div>
                <div class="modal-body">
                  <p>Request not sent! Try again later!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><a href="index.php">Close</a></button>
                </div>
              </div>
            </div>
          </div>
        <!--<script src="js/blurt.js"></script>-->
        <script>
            $('#successModal').modal({backdrop: 'static', keyboard: false}) 
        </script>
    </body>
</html>
<?php
if(empty($_SESSION['id']))
{
    echo '<script>location.href="Login.php";</script>';
}
$msgbody="Hiring Request!";
    include 'connection.php';
    $callNotify = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
    mysqli_stmt_bind_param($callNotify, 'iiss', $_SESSION['id'], $_GET['uid'], $msgbody,date("Y-m-d H:i:s"));
    if(!$callNotify)
    {
        echo "<script>$('#failModal').modal('show')</script>";
    }
 else {
        echo "<script>$('#successModal').modal('show')</script>";
    }
    mysqli_stmt_execute($callNotify);
?>