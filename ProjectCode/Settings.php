<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
?>
<html>
    <head>
        <title>AuditionMagic-Settings</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?> 
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Settings</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                           
                            <p>
                                <?php
                                if ($_SESSION['utype'] == "Artist") {
                                    ?>
                                    <strong><a href="changeProfilePhoto.php">Change Profile Photo</a></strong>
                                <hr>
                                <?php
                            }
                            ?>
                            <strong><a href="changePassword.php">Change Password</a></strong>
                            <hr>
                            <?php if($_SESSION['utype']=="Artist")
                            { ?>
                            <strong><a href="EditProfile.php">Edit Profile</a></strong>
                            <?php
                            }
                            else if($_SESSION['utype']=="Agent")
                            {
                            ?>
                            <strong><a href="EditProfileAgent.php">Edit Profile</a></strong>
                            <?php
                            }
                            else
                            {
                            ?>
                            <strong><a href="EditProfileDirector.php">Edit Profile</a></strong>
                            <?php
                            }
                            ?>
                            <hr>
                            <strong><a href="#" data-toggle="modal" data-target="#myModal">Deactivate Account</a></strong>
                            </p>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Deactivation</h4>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <p>You will activate your account again if you login!
                                             <br>  Are you sure you want to deactivate your account?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" name="Deactivate" class="btn btn-default" onclick="myFunction()">Deactivate</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </center>
                        </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
            <?php include 'footer.php'; ?> 
            <script>
            function myFunction() {
                window.location.href="Deactivate.php";
            }
           </script>
    </body>
</html>
