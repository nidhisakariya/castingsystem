<?php
session_start();
if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
$perror = $cperror = $print = "";
include 'connection.php';

if (isset($_POST['btn_submit'])) {
    $currentpassword = md5($_POST['currentpassword']);
    $newpassword = md5($_POST['newpassword']);
    $retypepassword = md5($_POST['retypepassword']);

    $query = "SELECT UserPassword FROM tbl_users where UserId='" . $_SESSION['id'] . "'";
    $result = mysqli_query($con,$query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $pass = $row['UserPassword'];

    if ($pass == $currentpassword) 
    {
            if ($newpassword == $retypepassword) 
            {
                $q = "UPDATE tbl_users SET UserPassword='" . $retypepassword . "' WHERE UserId='" . $_SESSION['id'] . "'";
                $update = mysqli_query($con, $q);

                if ($update)
                {
                    $print = '<div class="alert alert-success"><strong>Password changed successfully!</strong></div>';
                }
            } 
            else 
            {
                 $perror = "Password does not match!";
            }
    } 
    else 
    {
        $cperror = "Invalid  current password!";
    }
}
?>
    <html>
        <head>
            <title>Change Password</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        </head>
        <body>
            <div class="site-wrap">
    <?php
    if ($_SESSION['utype'] == "Director") {
        include 'Directorheader.php';
    } else if ($_SESSION['utype'] == "Agent") {
        include 'Agentheader.php';
    } else {
        include 'header.php';
    }
    ?> 
                <div class="site-mobile-menu">
                    <div class="site-mobile-menu-header">
                        <div class="site-mobile-menu-close mt-3">
                            <span class="icon-close2 js-menu-toggle"></span>
                        </div>
                    </div>
                    <div class="site-mobile-menu-body"></div>
                </div> <!-- .site-mobile-menu -->

                <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                     data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                    <div class="container">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                                <h1 class="text-white">Change Password</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="site-section">
                    <div class="container">
                        <div class="row">                           
                            <form method="post">
                                <?php echo $print; ?>
                                <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                                    <center>
                                        <div class="row form-group">
                                            <div class="col-md-12 mb-3 mb-md-0">
                                                <label class="font-weight-bold" for="currentpassword" style="color: #e3c4a8;">Enter current password:</label>
                                                <input type="password" name="currentpassword" class="form-control" style="color: black;width: 400px;" placeholder="Current password" required="required">
                                                <label style="color:red;"><?php echo $cperror; ?></label>
                                            </div><br><br>

                                            <div class="col-md-12 mb-3 mb-md-0">
                                                <label class="font-weight-bold" for="newpassword" style="color: #e3c4a8;">Enter new password:</label>
                                                <input type="password" name="newpassword" class="form-control" style="color: black;width: 400px;" placeholder="New password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required="required">
                                                <label style="color:red;"><?php echo $perror; ?></label>
                                            </div><br><br>

                                            <div class="col-md-12 mb-3 mb-md-0">
                                                <label class="font-weight-bold" for="retypepassword" style="color: #e3c4a8;">Retype password:</label>
                                                <input type="password" name="retypepassword" class="form-control" style="color: black;width: 400px;" placeholder="Retype password" required="required">
                                            </div>

                                        </div><br>

                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <input type="submit" value="Submit" name="btn_submit" class="btn btn-primary py-3 px-4">
                                            </div> 
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    <?php include 'footer.php'; ?> 
    </body>
</html>