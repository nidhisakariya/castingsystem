<?php
session_start();

if (empty($_SESSION['id'])) {
    echo '<script>location.href="Login.php";</script>';
}
else if($_SESSION['utype']!="Artist")
    {
        echo '<script>alert("You are not allowed to access this page");</script>';
        echo '<script>location.href="index.php"</script>';
    }
    else{
include 'connection.php';
    $id="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
    $select= mysqli_query($con, $id);
    $count= mysqli_num_rows($select);
    mysqli_close($con);
    if($count==0)
    {
        echo '<script>alert("You need to create profile before accessing this page");</script>';
        echo '<script>location.href="CreateProfile.php";</script>';
    }
    
    $Perror="";
    if(isset($_POST['btn_register']))
    {      
    $extension = pathinfo($_FILES["profile"]["name"], PATHINFO_EXTENSION);
    $name = "img" . $_SESSION['id'] . $_FILES["profile"]["name"];
    $targetDir = "ProfilePhotos/";
    $fileName = "$name";
    $targetFilePath = $targetDir . $fileName;
    $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
    $allowTypes = array('jpg', 'png', 'jpeg');
    
    if (!file_exists($_FILES["profile"]["tmp_name"])) {
        $Perror = "This file does not exist!";
    } else if (!in_array($extension, $allowTypes)) {
        $Perror = "Only jpg,jpeg and png files are allowed!";
    } else {
       include 'connection.php';
       $change="update tbl_artist_master set ProfilePhoto='".$name."' where UserId='".$_SESSION['id']."'";
       $setphoto=mysqli_query($con,$change);
       if($setphoto)
       {
            echo '<div class="alert alert-success"><strong>Profile photo changed successfully!</strong></div>';
        } else {
                    echo '<div class="alert alert-danger"><strong>Profile not changed some problem occured!'. mysqli_error($con).'</strong></div>';
        }   
       mysqli_close($con);
       
        move_uploaded_file($_FILES["profile"]["tmp_name"], $targetFilePath);
        
       // echo "<script>location.href='ArtistProfile.php';</script>";
    }
   }
}
?>
<html>
    <head>
        <title>Change Profile Photo</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="canonical" href="https://www.wrappixel.com/templates/adminwrap/"/>
        <link href="assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/colors/default.css" id="theme" rel="stylesheet">
        <script type="text/javascript">
            function update(str)
            {
                var xmlhttp;

                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("city").innerHTML = xmlhttp.responseText;
                    }
                }

                xmlhttp.open("GET", "fetchCity.php?opt=" + str, true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <div class="site-wrap">
        <?php include 'header.php'; ?>  
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Change Profile Photo</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center>
                        <form action="#" method="POST" class="contact-form" enctype="multipart/form-data">  
                            <div class="col-lg-6 col-md-6">
                                <label class="font-weight-bold" for="name" style="color: #e3c4a8;">Profile photo:</label>
                                <div class="card">
                                    <div class="card-body">
                                        <input type="file" name="profile" id="input-file-now" class="dropify"/>
                                    </div>
                                </div>
                                <label style="color: red"><?php echo $Perror; ?></label>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="reset" value="Reset" class="btn btn-primary py-3 px-4">&nbsp;
                                    <input type="submit" value="Change" name="btn_register" class="btn btn-primary py-3 px-4">
                                </div>
                            </div> 
                        </form>   
                    </center>
                </div>
            </div>
<?php include 'footer.php'; ?> 
            <script src="assets/node_modules/jquery/jquery.min.js"></script>
            <!-- Bootstrap tether Core JavaScript -->
            <script src="assets/node_modules/bootstrap/js/popper.min.js"></script>
            <script src="assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="js/sidebarmenu.js"></script>
            <!--stickey kit -->
            <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
            <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
            <!--Custom JavaScript -->
            <script src="js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- Plugins for this page -->
            <!-- ============================================================== -->
            <!-- jQuery file upload -->
            <script src="assets/node_modules/dropify/dist/js/dropify.min.js"></script>
            <script>
                                        $(function () {
                                            // Basic
                                            $('.dropify').dropify();

                                            // Translated
                                            $('.dropify-fr').dropify({
                                                messages: {
                                                    default: 'Glissez-déposez un fichier ici ou cliquez',
                                                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                                                    remove: 'Supprimer',
                                                    error: 'Désolé, le fichier trop volumineux'
                                                }
                                            });

                                            // Used events
                                            var drEvent = $('#input-file-events').dropify();

                                            drEvent.on('dropify.beforeClear', function (event, element) {
                                                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                                            });

                                            drEvent.on('dropify.afterClear', function (event, element) {
                                                alert('File deleted');
                                            });

                                            drEvent.on('dropify.errors', function (event, element) {
                                                console.log('Has Errors');
                                            });

                                            var drDestroy = $('#input-file-to-destroy').dropify();
                                            drDestroy = drDestroy.data('dropify')
                                            $('#toggleDropify').on('click', function (e) {
                                                e.preventDefault();
                                                if (drDestroy.isDropified()) {
                                                    drDestroy.destroy();
                                                } else {
                                                    drDestroy.init();
                                                }
                                            })
                                        });
            </script>
            <script src="assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
    </body>
</html>
