<?php
session_start(); 
include('database_connection.php');
$con = mysql_connect('localhost', 'root', '');
mysql_select_db('casting_system');

if($_SESSION['utype']=="Artist"){
    $getid="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
    $fetchid=mysql_query($getid);
    $id=mysql_fetch_Assoc($fetchid);   
?>
    
    <?php
    $query="Select distinct(AgentId) from tbl_clients where ArtistId='".$id['ArtistId']."'";
    $agents=mysql_query($query);
    $count=mysql_num_rows($agents);
    if($count!=0)
    {
    echo '<table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Agents</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($agents))
    {
        $select="select AgentName,UserId from tbl_agents_master where AgentId='".$fetch['AgentId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['AgentName'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['AgentName'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to agent from here after getting hired!";
    }
    ?>
    </table>
    <br><br>
    <?php
    $query="Select distinct(DirectorId) from tbl_applyjob where ArtistId='".$id['ArtistId']."' and SelectStatus=1";
    $agents=mysql_query($query);
    $count=mysql_num_rows($agents);
    if($count!=0)
    {
    echo '<table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Directors</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($agents))
    {
        $select="select CompanyName,UserId from tbl_castingdirector_master where DirectorId='".$fetch['DirectorId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['CompanyName'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['CompanyName'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to director from here after getting selected for auditions!";
    }
    ?>
     </table>        
<?php    
}
else if($_SESSION['utype']=="Agent") {
    $getid="select AgentId from tbl_agents_master where UserId='".$_SESSION['id']."'";
    $fetchid=mysql_query($getid);
    $id=mysql_fetch_Assoc($fetchid);
    
    $query="Select distinct(ArtistId) from tbl_clients where AgentId='".$id['AgentId']."'";
    $artists=mysql_query($query);
    $count=mysql_num_rows($artists);
    if($count!=0)
    {
    echo ' <table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Artists</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($artists))
    {
        $select="select Name,UserId from tbl_artist_master where ArtistId='".$fetch['ArtistId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['Name'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['Name'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to artist from here after getting hired!";
    }
    ?>
    </table>
    <br><br>
    <?php
    $query="Select distinct(DirectorId) from tbl_applyjob where AgentId='".$id['AgentId']."' and SelectStatus=1";
    $agents=mysql_query($query);
    $count=mysql_num_rows($agents);
    if($count!=0)
    {
    echo ' <table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Directors</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($agents))
    {
        $select="select CompanyName,UserId from tbl_castingdirector_master where DirectorId='".$fetch['DirectorId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['CompanyName'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['CompanyName'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to director from here after any of your artist gets selected for auditions!";
    }
    ?>
     </table>        
<?php
}
else {
    $getid="select DirectorId from tbl_castingdirector_master where UserId='".$_SESSION['id']."'";
    $fetchid=mysql_query($getid);
    $id=mysql_fetch_Assoc($fetchid);
   
    $query="Select distinct(ArtistId) from tbl_applyjob where DirectorId='".$id['DirectorId']."' and SelectStatus=1";
    $artists=mysql_query($query);
    $count=mysql_num_rows($artists);
    if($count!=0)
    {
    echo ' <table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Artists</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($artists))
    {
        $select="select Name,UserId from tbl_artist_master where ArtistId='".$fetch['ArtistId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['Name'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['Name'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to artists from here after you select them for auditions!";
    }
    ?>
    </table>
    <br><br>
    <?php
    $query="Select distinct(AgentId) from tbl_applyjob where DirectorId='".$id['DirectorId']."' and SelectStatus=1";
    $agents=mysql_query($query);
    $count=mysql_num_rows($agents);
    if($count!=0)
    {
    echo ' <table class="table table-bordered table-striped">
            <tr>
                    <th width="70%">Agents</td>
                    <th width="20%">Status</td>
                    <th width="10%">Action</td>
            </tr>';
    while($fetch= mysql_fetch_assoc($agents))
    {
        $select="select AgentName,UserId from tbl_agents_master where AgentId='".$fetch['AgentId']."'";
        $getnames= mysql_query($select);
        $row= mysql_fetch_assoc($getnames);
        $status = '';
        $current_timestamp = strtotime(date("Y-m-d H:i:s") . '- 10 second');
        $current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
        $user_last_activity = fetch_user_last_activity($row['UserId'], $connect);
        if($user_last_activity > $current_timestamp)
            {
                    $status = '<span class="label label-success">Online</span>';
            }
            else
            {
                    $status = '<span class="label label-danger">Offline</span>';
            }
            echo '
            <tr>
                    <td>'.$row['AgentName'].' '.count_unseen_message($row['UserId'], $_SESSION['id'], $connect).' '.fetch_is_type_status($row['UserId'], $connect).'</td>
                    <td>'.$status.'</td>
                    <td><button type="button" class="btn btn-primary py-3 px-4 start_chat" data-touserid="'.$row['UserId'].'" data-tousername="'.$row['AgentName'].'">Start Chat</button></td>
            </tr>';
    }
    }
    else
    {
        echo "You can talk to agents from here after you select their applications for auditions!";
    }
    ?>
    </table>
    <br><br>
<?php
}
?>