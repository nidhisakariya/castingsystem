<?php
session_start();
?>
<html>
    <head>
        <title>Forgot Password</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php include 'header.php'; ?> 
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Forgot Password</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">

                    <center class="title">
                        To reset your password, please enter your Casting Magic.com username. 
                        <br/>
                        Casting Magic.com will send the password reset instructions to the email address for this account.
                        <br/><br/>
                        If you don't know the username or your email address is no longer valid, please <a tabindex="3" href="ContactUs.php">Contact Us</a> for further assistance.
                    </center><br>
                    <center>
                        <div class="row">
                            <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                                <center>
                                    <form  method="POST">
                                        <div class="row form-group">
                                            <div class="col-md-12 mb-3 mb-md-0">
                                                <label class="font-weight-bold" for="userid" style="color: #e3c4a8;"">Email ID:</label>
                                                <input type="email" name="email" class="form-control" style="color: black;width: 400px;" placeholder="Email ID" required="required"><br><br>
                                                <input type="submit" value="Send OTP" name="btn_submit" class="btn btn-primary py-3 px-4">
                                            </div>

                                        </div>
                                    </form>
                                </center>
                            </div>
                        </div>
                    </center>
                    <?php
                    include 'connection.php';

                    if (isset($_POST['btn_submit'])) {
                        $_SESSION['userid'] = $_POST['email'];

                        $sql = "SELECT * FROM tbl_users WHERE UserName='" . $_POST['email'] . "'";
                        $result = mysqli_query($con, $sql);
                        $row = mysqli_fetch_array($result);
                        $mail = $row['UserName'];

                        if ($_POST['email'] == $mail) {
                            include 'OTP.php';
                            $_SESSION['fpswd'] = generateOTP($_POST['email']);
                            echo '<script> location.href="VerifyOTP.php"</script>';
                        } else {
                            echo "Invalid User";
                        }
                    }
                    ?>
                </div>
            </div>
                    <?php include 'footer.php'; ?> 
    </body>
</html>
