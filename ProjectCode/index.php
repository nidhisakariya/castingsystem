<?php
session_start();
if (!empty($_SESSION['uid'])) {
    $sess = 1;
} else {
    $sess = 0;
}
?>
<html>
    <head>
        <title>Index</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">

<?php
if ($sess == 1) {
    if ($_SESSION['utype'] == "Director") {
        include 'Directorheader.php';
    } else if ($_SESSION['utype'] == "Agent") {
        include 'Agentheader.php';
    }
    else {
    include 'header.php';
    }
} else {
    include 'header.php';
}
?>

            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover overlay" style="background-image: url('images/hero_bg_2.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="mb-4">Bringing Joy Through Reel</h1>
                            <p>
                            <?php
                            if ($sess == 1) {
                                ?>
                                    <a href="Logout.php" class="btn btn-primary px-5 py-3">Sign out</a>
                                <?php
                            } else {
                                ?>
                                    <a href="Login.php" class="btn btn-primary px-5 py-3">Sign in</a>
                                    <?php
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="image" style="background-image: url('images/Blockimage.jpg')"></div>
                    <div class="text">
                        <h2>Artists </h2>
                        <p class="mb-5">
                            Search more jobs than any other platform and get casted fast. Flaunt your best headshots and present your performance skills to thousands of casting directors. 
                            No restrictions on the number of jobs you can apply for!
                        </p>
                        <p><a href="HowArtist.php" class="btn btn-primary px-4 py-3">Get Details</a></p>
                        <br>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="text">
                        <h2>Casting Cells </h2>
                        <p class="mb-5">
                            Use the India's most sophisticated casting platform. Manage projects and organize your casting notices for each production. Proactively invite your favorite performers to audition for your project.
                        </p>
                        <p><a href="HowDirector.php" class="btn btn-primary px-4 py-3">Get Details</a></p>
                        <br>
                    </div>
                    <div class="image" style="background-image: url('images/Casting.jpg')"></div>
                </div>
            </div>

            <div class="container">
                <div class="featured-property-half d-flex">
                    <div class="image" style="background-image: url('images/agent.jpg')"></div>
                    <div class="text">
                        <h2>Agents </h2>
                        <p class="mb-5">Manage your roster clients' profiles and submit them to thousands of projects posted by the India's most prolific casting directors.  </p>
                        <p><a href="HowAgent.php" class="btn btn-primary px-4 py-3">Get Details</a></p>
                        <br>
                    </div>
                </div>
            </div>
<?php include 'footer.php'; ?>
    </body>
</html>
