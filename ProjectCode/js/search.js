$(document).ready(function(){
    filterSearch();	
    $('.productDetail').click(function(){
        filterSearch();
    });	 	
});
function filterSearch() {
	$('.searchResult').html('<div id="loading">Loading .....</div>');
	var action = 'fetch_data';
	var Gender = getFilterData('Gender');
	var BodyShape = getFilterData('BodyShape');
	var HairColor = getFilterData('HairColor');
        var EyeColor = getFilterData('EyeColor');
        var Skills = getFilterData('Skills')
	$.ajax({
		url:"action.php",
		method:"POST",
		dataType: "json",		
		data:{action:action, Gender:Gender, BodyShape:BodyShape, HairColor:HairColor, EyeColor:EyeColor, Skills:Skills},
		success:function(data){
			$('.searchResult').html(data.html);
		}
	});
}
function getFilterData(className) {
	var filter = [];
	$('.'+className+':checked').each(function(){
		filter.push($(this).val());
	});
	return filter;
}