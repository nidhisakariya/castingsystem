$(document).ready(function(){
    filterSearch();	
    $('.productDetail').click(function(){
        filterSearch();
    });	 	
});
function filterSearch() {
	$('.searchResult').html('<div id="loading">Loading .....</div>');
	var action = 'fetch_data';
	var CompanyName = getFilterData('CompanyName');
	var CityId = getFilterData('CityId');
	var ExistingSince = getFilterData('ExistingSince')
	$.ajax({
		url:"actiondir.php",
		method:"POST",
		dataType: "json",		
		data:{action:action, CompanyName:CompanyName, CityId:CityId, ExistingSince:ExistingSince},
		success:function(data){
			$('.searchResult').html(data.html);
		}
	});
}
function getFilterData(className) {
	var filter = [];
	$('.'+className+':checked').each(function(){
		filter.push($(this).val());
	});
	return filter;
}