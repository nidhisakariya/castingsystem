<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php 
session_start();
if(empty($_SESSION['userid']))
{
    echo '<script>location.href="Login.php"</script>';
}
$Perror="";
            include 'connection.php';
            if (isset($_POST['btn_submit'])) {
                if ($_POST['newpassword'] == $_POST['rpassword']) 
                    {
                    $newpassword=md5($_POST['newpassword']);
                    
                    $q="Update tbl_users Set UserPassword='".$newpassword."' where UserName='".$_SESSION['userid']."' ";
                    $result= mysqli_query($con, $q);
                    
                    if($result==1)
                    {
                         echo '<script>location.href="Login.php"</script>';
                    } 
                    }
                else 
                {
                    $Perror="Entered password does not match !";
                }
            }
            ?>
<html>
    <head>
        <title>New Password</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
            <?php 
                include 'header.php';
            ?> 
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">New Password</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <div class="row">

                        <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                            <center>
                                <form action="#" method="POST" class="row form-group">
                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="newpassword1" style="color: #e3c4a8;">Enter new password:</label>
                                        <input type="password" name="newpassword" class="form-control" style="color: black;width: 400px;" placeholder="New password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required="required">
                                    </div><br><br>

                                    <div class="col-md-12 mb-3 mb-md-0">
                                        <label class="font-weight-bold" for="rpassword1" style="color: #e3c4a8;">Retype password:</label>
                                        <input type="password" name="rpassword" class="form-control" style="color: black;width: 400px;" placeholder="Retype password" required="required"><br>
                                        <label style="color:red;"><?php echo $Perror; ?></label>
                                        <input type="submit" value="Set Password" name="btn_submit" class="btn btn-primary py-3 px-4">
                                    </div><br><br>                     
                                    <br>

                                     <div class="row form-group">
                                        <div class="col-md-12">
                                            
                                        </div> 
                                    </div>
                                </form>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'footer.php'; ?> 
    </body>
</html>