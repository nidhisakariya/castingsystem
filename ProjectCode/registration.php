<?php
session_start();
$cateerror="";
if (isset($_POST['btn_submit'])) {
    if($_POST['category']=="Select your category")
    {
        $cateerror="Please select your category!";
    }
    else {
        include 'connection.php';
        $query="select UserName from tbl_users where UserName='".$_POST['emailid']."'";
        $result= mysqli_query($con, $query);
        $count=mysqli_num_rows($result);
        if($count==0)
        {
            include 'OTP.php';
            $_SESSION["otp"] = generateOTP($_POST['emailid']);
            $_SESSION["user"] = $_POST['category'];
            $_SESSION["email"] = $_POST['emailid'];
            echo "<script> location.href='registration1.php'; </script>";   
        }
        else {
            echo "<script>alert('This email is already registered');</script>";
        }
    }
}
?>
<html>
    <head>
        <title>Registration</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    </head>
    <body style="background-image:url('images/login1.jpg');background-size: 100%;background-repeat: repeat-x; ">
        <div class="site-wrap">
            <?php include 'header.php'; ?>
            <br>
            <br>
            <div class="site-section ">
                <div class="container">
                    <form action="#" method="POST" class="contact-form">   
                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="usertype" style="color: white;">I am:</label>
                                <select class="form-control" style="color: white;width: 400px;" name="category" required="required">
                                    <option style="background-color:burlywood;color: white;">Select your category</option>
                                    <option style="background-color:burlywood;color: black;">Artist</option>
                                    <option style="background-color:burlywood;color: black;">Casting Director</option>
                                </select>
                                <label style="color:red;"><?php echo $cateerror; ?></label>
                            </div>
                        </div>
                        <br> 
                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="email" style="color: white;">Enter Email Id:</label>
                                <input type="email" name="emailid" class="form-control" style="color: white;width: 400px;" placeholder="Email Id" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Please enter valid email id">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Submit" name="btn_submit" class="btn btn-primary py-3 px-4">
                            </div>
                        </div>
                    </form>       
                </div>
            </div>
        </div>
    </body>
</html>