<?php
session_start();
if(empty($_SESSION['email']))
{
    echo "<script> location.href='registration.php'; </script>";
}
$otperror="";
            if (isset($_POST['btn_register'])) {
                if ($_POST['otpassword'] == $_SESSION["otp"]) {
                    if ($_SESSION["user"] == "Casting Director") {
                        $s= $as = 1;
                        $u = "Director";
                        include 'connection.php';
                        $call = mysqli_prepare($con, 'CALL register(?, ?, ?, ?, ?)');
                        mysqli_stmt_bind_param($call, 'sssii', $_SESSION["email"], md5($_POST['pswd']), $u, $s, $as);
                        mysqli_stmt_execute($call);
                        echo "<script> location.href='Login.php'; </script>";
                    } 
                     else {
                        $_SESSION['pswd'] = md5($_POST['pswd']);
                        echo "<script> location.href='payment.php'; </script>";
                    }
                } 
                else {
                    $otperror="Incorrect OTP!";
                }
            }
         ?>
<html>
    <head>
        <title>Registration</title>
        <meta charset="utf-8">    
    </head>
    
    <body>
    <body style="background-image:url('images/login1.jpg');background-size: 100%;background-repeat: repeat-x; ">
        <div class="site-wrap">

            <?php
            include 'header.php';
            echo $_SESSION["otp"];
            ?>
            <br>
            <br>
            <div class="site-section ">
                <div class="container">
                    <form action="#" method="POST" class="contact-form">   
                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="otpgen" style="color: white;">Enter OTP send on your email:</label>
                                <input type="text" name="otpassword" class="form-control" style="color: white;width: 400px;" placeholder="OTP" required="required">
                                <label style="color:red;"><?php echo $otperror;?></label>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12 mb-3 mb-md-0">
                                <label class="font-weight-bold" for="pswd" style="color: white;">Enter Password:</label>
                                <input type="password" name="pswd" class="form-control" style="color: white;width: 400px;" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="Password" required="required">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Register" name="btn_register" class="btn btn-primary py-3 px-4">
                            </div>
                        </div> 
                    </form>       
                </div>
            </div>
    </body>
</html>