<?php
session_start();
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script>
            function redirection()
            {
                window.location = "Login.php";
            }
        </script>
    </head>

    <body>
        <?php
        $status = $_POST["status"];
        $firstname = $_POST["firstname"];
        $amount = $_POST["amount"];
        $txnid = $_POST["txnid"];
        $posted_hash = $_POST["hash"];
        $key = $_POST["key"];
        $productinfo = $_POST["productinfo"];
        $email = $_POST["email"];
        $salt = "ebGnMmC9Kd";

// Salt should be same Post Request
        If (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $hash = hash("sha512", $retHashSeq);
        if ($hash != $posted_hash) {
            echo "Invalid Transaction. Please try again";
        } else {
            if(empty($_SESSION['AgentAccepted']))
            {
                $s = $as = 1;
                $u = "Artist";
                include 'connection.php';
                $callregister = mysqli_prepare($con, 'CALL register(?, ?, ?, ?, ?)');
                mysqli_stmt_bind_param($callregister, 'sssii', $_SESSION['email'], $_SESSION['pswd'], $u, $s, $as);
                mysqli_stmt_execute($callregister);

                $sql = "SELECT UserId FROM tbl_users WHERE UserName = '" . $_SESSION["email"] . "'";
                $result = mysqli_query($con, $sql);
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $count = mysqli_num_rows($result);
                
                $amount = "5000";
                if ($count == 1) {
                    $call1 = mysqli_prepare($con, 'CALL payment(?, ?, ?)');
                    mysqli_stmt_bind_param($call1, 'sii', date("Y-m-d"), $row['UserId'], $amount);
                    mysqli_stmt_execute($call1);
                    echo '<script>location.href="Login.php";</script>';
                }
                else {
                    $print='<div class="alert alert-danger"><strong>Something went wrong!Please try again later!</strong></div>';
                }
                mysqli_close($con);
            }
            else {
                include 'connection.php';
                $query="select ArtistId from tbl_artist_master where UserId='".$_SESSION['id']."'";
                $result=mysqli_query($con,$query);
                $row= mysqli_fetch_array($result,MYSQLI_ASSOC);
                
                $callClient = mysqli_prepare($con, 'CALL CreateClient(?, ?)');
                mysqli_stmt_bind_param($callClient, 'ii', $row['ArtistId'], $_SESSION['AgentAccepted']);
                mysqli_stmt_execute($callClient);
                
                mysqli_close($con);
                include 'connection.php';
                $callPayment = mysqli_prepare($con, 'CALL ArtistPayment(?, ?, ?, ?)');
                mysqli_stmt_bind_param($callPayment, 'iiis', $row['ArtistId'], $_SESSION['AgentAccepted'],$_SESSION['Amount'],date("Y-m-d"));
                mysqli_stmt_execute($callPayment);
                
                mysqli_close($con);
                $msg="Accepted your request as their agent!";
                include 'connection.php';
                $callnotify = mysqli_prepare($con, 'CALL notify(?, ?, ?, ?)');
                mysqli_stmt_bind_param($callnotify, 'iiss',$_SESSION['id'], $_SESSION['AgentUser'],$msg,date("Y-m-d H:i:s"));
                mysqli_stmt_execute($callnotify);
                echo '<script>location.href="ArtistRoaster.php";</script>';
                mysqli_close($con);
            }
        }
        ?>
        <div class="site-wrap">
    <?php include 'header.php'; ?>    
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
    data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white">Registration</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="site-section">
    <div class="container">
      <div class="row">
        
          <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
              <p>
                  <?php echo $print;?>
              </p>
        </div>
        </div>
    </div>
  </div>
    <?php include 'footer.php'; ?> 
    </body>
</html>