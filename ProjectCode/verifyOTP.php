<?php
session_start();
$perror = "";
if (isset($_POST['btn_submit'])) {
    if ($_POST['otp'] == $_SESSION['fpswd']) {
        echo '<script>location.href="NewPassword.php"</script>';
    } else {
        $perror = "Invalid OTP!";
    }
}
?>
<html>
    <head>
        <title>Verify OTP</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
        <div class="site-wrap">
<?php include 'header.php'; ?> 
            <div class="site-mobile-menu">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div> <!-- .site-mobile-menu -->

            <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/background.png');"
                 data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 text-center" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white">Forgot Password</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="site-section">
                <div class="container">
                    <center class="title">
                        The OTP has been sent to your Email address. <br>
                        Enter the OTP below to get it verified.

                    </center><br>

                    <div class="row">
                        <div class="site-section-heading text-center mb-5 w-border col-md-6 mx-auto">
                            <center>
                                <form action="#" method="post">
                                    <div class="row form-group">
                                        <div class="col-md-12 mb-3 mb-md-0">
                                            <label class="font-weight-bold" for="otp1" style="color: #e3c4a8;">Enter your OTP:</label>
                                            <input type="text" name="otp" class="form-control" style="color: black;width: 400px;" placeholder="Enter OTP" required="required">
                                            <label style="color:red;"><?php echo $perror; ?></label><br><br>
                                            <input type="submit" value="Verify OTP" name="btn_submit" class="btn btn-primary py-3 px-4">
                                        </div><br><br>
                                    </div>
                                </form>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
<?php include 'footer.php'; ?> 
    </body>
</html>
